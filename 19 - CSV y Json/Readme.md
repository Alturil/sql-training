# CSV y JSON

## CSV

CSV (_Comma Separated Values_) es un formato muy popular que permite representar una tabla en un archivo de texto plano que consiste de una serie de valores delimitados por caracteres de control; usualmente:

- Coma para separar columnas (`,`)
- Salto de linea para separar filas (`\n`)
- Opcionalmente la primer fila se interpreta como el nombre de las columnas

**E.j:**

```
nombre,apellido,edad
Alturil,De Gondor,33
Roberto,Goyeneche,78
Juan Carlos,Batman,45
```

Cabe destacar que si bien es frecuente utilizar comas, los caracteres de control son completamente arbitrarios, y la mayoria de las plataformas y lenguajes de programacion permiten especificar el caracter utilizado.

## JSON

JSON (JavaScript Object Notation) es otra forma de representar datos, pero en forma denormalizada y con la flexibilidad de poder anidar objetos y listas.

- Se utilizan llaves para delimitar objetos (`{}`)
- Las propiedades de un objeto se expresan en pares de _key_ y _value_, utilizando comillas para _key_ y comillas para _values_ que no sean numericos.
- Colecciones con elementos del mismo tipo se delimitan con corchetes (`[]`)
- Los objetos y las colecciones pueden anidarse

El set anterior puede representarse en Json de la siguiente forma:

```json
[
    {
        "nombre" : "Alturil",
        "apellido" : "De Gondor",
        "edad" : 33
    },
    {
        "nombre" : "Roberto",
        "apellido" : "Goyeneche",
        "edad" : 78
    },
    {
        "nombre" : "Juan Carlos",
        "apellido" : "Batman",
        "edad" : 45
    }
]
```

Esta forma de notacion se utiliza casi universalmente en aplicaciones, APIs, etc, por lo cual resulta util estar familiarizados con formato para poder importar y exportar datos facilmente.

## Utilidades

La extension de SQL Server de Visual Studio Code permite exportar un result set como JSON o CSV simplemente haciendo click con el boton derecho en un result set.

Hay utilidades en internet que permiten convertir CSV y Json a instrucciones de SQL:

- https://www.convertcsv.com/csv-to-sql.htm
- https://www.convertjson.com/json-to-sql.htm