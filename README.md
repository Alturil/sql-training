# SQL Training

**Note:** This repository has been migrated from GitHub. The original project can be found [HERE](https://github.com/Alturil/SqlStuff)

## Clases

Cada clase se encuentra en su respectiva carpeta, a donde el contenido teorico parece en archivos `readme.md` y los scripts auxiliares aparecen en la misma carpeta (usualmente archivos `.sql`).

## Tarea

Vamos a utilizar la siguiente estrategia:

![branching strategy](gitbranch.png)

### Git paso a paso

Nos cambiamos a `master`, y hacemos un `pull` para bajarnos los ultimos cambios del repositorio remoto:

```
git checkout master; git pull
```

Luego nos cambiamos a nuestro branch de tarea principal, y mergeamos los cambios de `master`:

```
git checkout branch-tarea; git merge master
```

Luego creamos nuestro branch y lo establecemos como espacio de trabajo:

```
git checkout -b tarea-clase-x-minombre
```

Ahora agregamos la tarea en un archivo `.md`, y una vez que finalizamos:

```
git add . ; git commit -m "Tarea de la clase x"
```

Y publicamos el branch en el repositorio remoto, creando un branch con el mismo nombre para trackear los cambios del repositorio local:

```
git push --set-upstream origin tarea-clase-x-minombre
```

Una vez finalizado, vamos a la pagina de GitLab y creamos un Merge Request (desde "Merge Requests" en el menu de la izquierda)

Elegimos:

- El branch que publicamos como branch de origen (en mi caso: `tarea-clase-x-minombre`)
- El branch de tarea principal como branch de destino (en mi caso: `branch-tarea`)

Volia!

## Tutoriales para aprender y practicar git

- [**Git Real**](https://www.pluralsight.com/courses/code-school-git-real) - Video tutorial con ejercicios, muy bien explicado y con una presentacion impresionante:
- [**KataCoda**](https://www.katacoda.com/courses/git/) - Consola interactiva con ejercicios paso a paso
- [**LearnGitBranching**](https://learngitbranching.js.org/) - Consola interactiva, pero mas limitada, con dibujitos
- [**Fracz**](https://gitexercises.fracz.com/) - Repositorio con ejercicios, utilizando la consola exclusivamente (bash)