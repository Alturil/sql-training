-- First version

DROP TABLE member;

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[member](
	[member_id] [int] IDENTITY(1,1) NOT NULL,
	[first_name] [nvarchar](1000) NULL,
	[last_name] [nvarchar](1000) NULL,
	[date_of_birth] [date] NULL,
	[nationality] [nvarchar](1000) NULL	
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[member] ADD  CONSTRAINT [PK_member] PRIMARY KEY CLUSTERED 
(
	[member_id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY]
GO


INSERT INTO dbo.member(first_name,last_name,date_of_birth,nationality)
VALUES
('Jonatan', 'Jaworski', '1987-09-29', 'Argentina'),
('Mirtha', 'Legrand', '1205-07-15', 'Bizantine Empire'),
('Sergio', 'Denis', '1962-02-01', 'argentina'),
('Teto', 'Medina', '1962-05-27', 'Uruguayy'),
('Roberto', 'Carlos', '1955-03-17', 'Brasil'),
('Jamiro', 'Quai', '1990-11-20', 'USA'),
('Azucar', 'Amargo', '1975-01-30', 'Venezuela'),
('Topo', 'Yiyo', '1962-12-14', 'URSS'),
('Juan Carlos', 'Batman', '1960-04-27', 'United States'),
('Juan Domingo', 'Peron', '1974-03-14', 'Portugal'),
('Pepe', 'Biondi', '1957-07-14', 'Spain'),
('Noam', 'Chomsky', '1995-04-17', 'Italy'),
('Horacio', 'Guarani', '1987-12-02', 'Italy'),
('Silvio', 'Soldan', '1992-10-01', 'Uruguay'),
('Susana', 'Gimenez', '1989-01-23', 'Japan'),
('Valeria', 'Lynch', '1975-02-01', 'New Zealand'),
('Susan', 'Sarandonga', '1982-12-12', 'Australia'),
('Joseph', 'Stalin', '1977-06-09', 'Georgia')