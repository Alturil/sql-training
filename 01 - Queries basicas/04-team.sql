DROP TABLE team;

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[team](
	[team_id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](1000) NOT NULL,
	[conference_id] [int] NULL,
    [country_id] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[team] ADD  CONSTRAINT [PK_team] PRIMARY KEY CLUSTERED 
(
	[team_id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY]
GO


INSERT INTO dbo.team([name], [conference_id], [country_id])
VALUES
('Brumbies',1,9),
('Rebels',1,9),
('Reds',1,9),
('Sunwolves',1,85),
('Waratahs',1,9),
('Blues',2,127),
('Chiefs',2,127),
('Crusaders',2,127),
('Highlanders',2,127),
('Hurricanes',2,127),
('Bulls',3,162),
('Jaguares',3,7),
('Lions',3,162),
('Sharks',3,162),
('Stormers',3,162);