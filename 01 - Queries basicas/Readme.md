# Clase 1 - Queries basicas

## 1. Comentarios

Los comentarios no se ejecutan, asi que sirven para dejar aclaraciones o instrucciones:

```sql
-- Esta linea esta comentada, no se ejecuta
SELECT * FROM member; -- Esta parte de la linea tampoco!

/*
Esto es un comentario en bloque.
Todas las lineas entre medio no se ejecutan.
*/
```

## 2. Query basica

```sql
SELECT <tabla.columna>, <tabla.columna>, ...
FROM <tabla>
WHERE <condicion>
ORDER BY <criterio>
```

Ejemplo:

```sql
SELECT first_name, last_name
FROM member
WHERE nationality_id IN (1, 57, 42)
ORDER BY first_name ASC
```

## 3. Operadores

En esta seccion se pueden utilizar distintos operadores:

- `=` igual. Ej: ```WHERE first_name = 'Sergio'```
- `<>` distinto. Ej: `WHERE country <> 'Argentina'`
- `>` mayor: Ej: `WHERE date_of_birth > '1980-01-01'`
- `<` menor: Ej: `WHERE member_id < 4`
- `IN ()` igual a algun miembro de la lista. Ej: `WHERE nationality_id IN (1,45,87)`
- `NOT IN ()` distinto a todos los miembros de la lista. Ej: `WHERE nationality_id NOT IN (1,45,87)`

## 4. TOP

Es posible limitar la cantidad de resultados usando TOP

Ejemplo: seleccionar los primeros 10 miembros:

```sql
SELECT TOP 10
FROM member
```

Esto es util combinado con `ORDER BY`.
Ejemplo: quien es el miembro mas viejo?

```sql
SELECT TOP 1
FROM member
ORDER BY date_of_birth ASC
```

## 4. Inner joins

Vamos a usar dos conceptos fundamentales:

- La columna que contiene el id unico de una tabla se denomina `primary key`.
- La columna que contiene el id que hace referencia a una `primary key` en otra tabla se denomina `foreign key`.

```sql
SELECT <tabla1.columna>, <tabla2.columna>, ...
FROM <tabla1>
JOIN <tabla2> ON <tabla1.foreign_key> = <tabla2.primary_key>
```

Ejemplo:

```sql
SELECT member.first_name, member.last_name, country.country
FROM member
JOIN country ON member.nationality_id = country.country_id;
```

# Homework

Fijarse el nuevo diagrama en https://dbdiagram.io/d/5e01598cedf08a25543f5e49.

- Ahora cada miembro es hincha de un equipo de Super Rugby. Los equipos estan la tabla `team`.
- Como es un torneo internacional, hay equipos de varios paises, asi que hay una relacion entre `team` y `country`.
- Por ultimo, los equipos se reparten en tres conferencias, asi que hay una relacion entre `team` y `conference`.

## 1. Ejercicios con una tabla

1. Selccionar apellido y nombre de los tres miembros mas viejos
2. Seleccionar todos los hinchas de Hurricanes
3. Seleccionar los cuatro ultimos miembros, juzgando por su apellido
4. Seleccionar todos los miembros nacidos antes del 1 de Enero de 1990 (guarda con el formato: `yyyy-mm-dd`)

## 2. Ejercicios con joins

1. Seleccionar nombre, apellido, y nombre del equipo de que son hinchas
2. Seleccionar todos los equipos y sus respectivas conferencias.
3. Seleccionar todos los equipos y sus respectivos paises.

