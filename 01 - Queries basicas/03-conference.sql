DROP TABLE conference;

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[conference](
	[conference_id] [int] IDENTITY(1,1) NOT NULL,
	[conference] [nvarchar](1000) NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[conference] ADD  CONSTRAINT [PK_conference] PRIMARY KEY CLUSTERED 
(
	[conference_id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY]
GO


INSERT INTO dbo.conference([conference])
VALUES
('Australian'), ('New Zealand'),('South African');