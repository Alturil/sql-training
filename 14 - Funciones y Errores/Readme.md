# Funciones y errores

## Funciones

Las funciones tienen como objetivo encapsular una serie de comandos que retorna un resultado, a diferencia de un stored procedure que no necesariamente devuelve un resultado.

Vamos a ver por ahora unicamente las funciones denominadas `scalar function`, que devuelven un valor unico.

### Crear una funcion

Para crear una funcion, la sintaxis es muy parecida a un stored procedure; la principal diferencia es que una funcion siempre devuelve un tipo de valor predeterminado.

```sql
CREATE FUNCTION [nombre_de_la_funcion]
(
    [parametros]
)
RETURNS [tipo_de_dato]
BEGIN
    [comandos]
    RETURN [valor]
END
```

**Ej:** Devolver la cantidad de armas que posee un personaje:

```sql
CREATE FUNCTION [get_character_weapon_count]
(
    @character_name NVARCHAR(1000)
)
RETURNS INT
BEGIN
    
    DECLARE @character_id INT = 
        (SELECT character_id FROM [character]
        WHERE [name] = @character_name);
    
    DECLARE @weapon_count INT =
        (SELECT COUNT(1) FROM character_weapon
        WHERE character_id = @character_id);

    RETURN @weapon_count;

END
```

### Ejecutar una funcion

Al devolver un valor unico, las funciones pueden ser utilizadas al igual que un `SELECT`. La unica diferencia es que para invocar una funcion hay que consignar el esquema; a fines practicos, asumimos que el esquema es `dbo` (Database Owner).

Los parametros se consignan entre parentesis, separados por coma en lugar de ser mas de uno, siguiendo el orden estricto en el que estan declarados en la defincion de la funcion (es decir, sin utilizar nombres de parametros).

**Ej:** Ejecutar la funcion `get_character_weapon_count` para obtener el conteo de armas de `Alturil`:

```sql
SELECT dbo.get_character_weapon_count('Alturil');
```

Es posible expandir el uso de funciones para extender una consulta con datos que no estan en ninguna tabla en particular pero que pueden ser calculados.

**Ej:** Devolver datos varios sobre personajes, incluyendo la cantidad de armas que poseen:

```sql
SELECT
    ch.[name] 'Name',
    c.class 'Class',
    r.race 'Race',
    dbo.get_character_weapon_count(ch.[name]) 'Weapons'
FROM [character] ch
JOIN race r ON ch.race_id = r.race_id
JOIN class c ON ch.class_id = c.class_id;
```

### Modificar una funcion

La sintaxis es exactamente la misma que `CREATE`, pero utilizando `ALTER` como verbo en lugar de `CREATE`, del mismo modo que un stored procedure.

### Eliminar una funcion

Se utiliza `DROP FUNCTION`, muy similar a un stored procedure:

**Ej:** Borrar la funcion `get_character_weapon_count`:

```sql
DROP FUNCTION get_character_weapon_count;
```

## Errores

Es una buena practica diseñar las consultas de modo tal que sean lo suficientemente robustas para manejar eficientemente errores de uso y reportar fallasd adecuadamente.

Si bien existen muchas categorias de errores, vamos a enfocarnos en tres posibilidades que queremos gestionar:

- Los datos referenciados no existen
- Los datos a insertar ya existen
- Reglas de negocio

Ejemplos de lo anterior podrian ser:

- Proveer como parametro de entrada en un stored procedure o funcion el nombre de un personaje que no existe.
- Proveer como parametro de entrada en un stored procedure o funcion que agrega razas, el nombre de una raza que ya existe.
- Asignarle un valor numerico negativo a la experiencia de un personaje (el minimo es 0).

### `RAISERROR`

La forma en que comunicamos un error al usuario es a traves del comando `RAISERROR`. La sintaxis basica es:

```sql
RAISERROR ([mensaje], [severidad], [estado])
```

A donde:

- `[mensaje]` es el texto de error a comunicar.
- `[severidad]` indica la gravedad del error.
- `[estado]` es un identificador interno.

A fines practicos, vamos a adoptar la siguiente convencion:

- Los mensajes de error deben comunicar la causa que invalida la transaccion. Ej: "El personaje indicado no existe".
- Vamos a establecer severidad `11` para todos los errores (dentro del rango de nivel 11-18).
- Vamos a utilizar `1` para todos los estados.

Ejemplo:

```sql
RAISERROR ( 'El personaje indicado no existe.',11,1);
```

### Condicionales y `RETURN`

Podemos utilizar `IF` para ejecutar un bloque de comandos unicamente si una condicion se cumple:

```sql
IF [condicion]
BEGIN
    [comandos]
END
```

Un uso muy comun es combinarlo con la expresion `EXISTS` y `NOT EXISTS`, que nos permiten identificar si la expresion a evaluar devuelve filas o no:

```sql
IF [EXISTS|NOT EXISTS]([expresion])
BEGIN
    [comandos]
END
```

Adicionalmente, en un stored procedure es posible utilizar el comando `RETURN` para abortar la ejecucion del stored procedure inmediatamente, sin ejecutar el codigo restante.

**Ej:** Devolver la cantidad de armas que posee un personaje, pero tirar un error si el personaje no existe:

```sql
CREATE PROCEDURE [get_character_weapon_count]
(
    @character_id INT
)
AS
    IF NOT EXISTS (SELECT * FROM [character]
        WHERE [character_id] = @character_id)
    BEGIN
        RAISERROR ( 'El personaje indicado no existe.',11,1);
        RETURN;
    END
       
    SELECT COUNT(1) FROM character_weapon
    WHERE character_id = @character_id;

GO
```

Utilizar `RETURN` sin un valor no es posible en una funcion escalar, ya que esta requiere que el valor de retorno sea del tipo especificado en la declaracion (`RETURNS [tipo_de_dato]`).