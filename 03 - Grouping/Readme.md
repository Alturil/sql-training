# 03. Grouping

## 01. JOINs utilizando NULL

Si combinamos los tipos de `JOIN` que ya conocemos con los filtros de valores `NULL` (Es decir: `WHERE <columna> IS NULL` y `WHERE <columna> IS NOT NULL`), es posible extraer los tipos de segmentos restantes en un diagrama de Venn:

![joins](https://i.stack.imgur.com/66zgg.png "Diferentes joins")

**Ej:** Seleccionar los equipos que NO tengan hinchas:

```sql
SELECT t.name 'Teams with no supporters'
FROM member m
RIGHT JOIN team t ON m.supported_team_id = t.team_id
WHERE m.supported_team_id IS NULL;
```

## 01. Modificador DISTINCT

En caso de seleccionar un grupo que devuelve una lista con valores repetidos, es posible remover los duplicados usando el modificador `DISTINCT`.

**E.g:** Devolver una lista de equipos que tienen hinchas:

```sql
SELECT DISTINCT t.name 'Teams'
FROM member m
JOIN team t ON m.supported_team_id = t.team_id;
```

## 02. Funcion COUNT

Se pueden contar elementos en un grupo usando la funcion `COUNT`.
La funcion toma como parametro una expresion que determina que y como contar. Generalmente usamos:

- `COUNT(*)`: Cuenta todas las filas (sin importar sus valores, incluyendo aquellas que sean `NULL`).
- `COUNT(<columna>)`: Cuenta la cantidad de filas que tienen un valor asignado en la columna especificada (es decir, que no sean `NULL`).

**Ej:** Contar la cantidad de filas en la tabla `member`:

```sql
SELECT COUNT(*) 'Total members'
FROM member;
```

**Ej:** Contar la cantidad de miembros que son hinchas de algun equipo:

```sql
SELECT COUNT(supported_team_id) 'Total supporters'
FROM member;
```

Es posible tambien utilizar el modificador `DISTINCT` en la expresion que toma la funcion `COUNT`:

**Ej:** Contar la cantidad de equipos que tienen hinchas:

```sql
SELECT COUNT(DISTINCT supported_team_id) 'Supported teams'
FROM member;
```

## 03. Clause GROUP BY

A la hora de extraer informacion agregada (_aggregations_), una de los principales aspectos es la agrupacion. Para eso se utiliza `GROUP BY`, que permite devolver una sola fila que contiene registros con un mismo valor.

El principal uso es aplicar funciones a un grupo para extraer distribuciones, por ejemplo utilizando `COUNT`.

**Ej:** Contar la cantidad de hinchas que tiene cada equipo:

```sql
SELECT t.name 'Team', COUNT(member_id) 'Supporters'
FROM member m
RIGHT JOIN team t ON m.supported_team_id = t.team_id
GROUP BY t.name
ORDER BY COUNT(member_id) DESC
```

Es importante que todas las columnas que deban ser agrupadas deben estar contenidas en la clausula `ORDER BY`. Solamente pueden quedar afuera aquellas que devuelvan un valor unico que aplica a todo el grupo (en el ejemplo esta es la funcion `COUNT`).

# Homework

1. Cuantos miembros hay nacidos antes del 2 de Febrero de 1975?
2. Cuantos equipos hay en cada conferencia?
3. Cual es el equipo que tiene mas hinchas?
4. Cuantos hinchas hay en cada conferencia?
5. Cuantos equipos sin hinchas tiene cada conferencia?