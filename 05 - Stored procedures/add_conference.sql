-- DESCRIPTION:
-- Creates stored procedure [dbo].[add_conference]

CREATE OR ALTER PROCEDURE [dbo].[add_conference]
(
	@conference_name NVARCHAR(1000)
)
AS
	SET NOCOUNT ON;
	
	INSERT INTO [dbo].[conference] (conference)
	SELECT @conference_name;
	
	-- Return created entity
	SELECT @@IDENTITY 'Conference id';
		
	SET NOCOUNT OFF;
GO

-- End of the script
