# Stored procedures

Un stored procedure es un grupo de comandos agrupados que pueden ejecutarse a traves de un simple llamado.

Utilizar stored procedure tiene varias ventajas, pero las principales que vamos a ver nosotros son:

- Reusabilidad: evita repetir codigo de forma innecesaria
- Mantenibilidad: facilita la actualizacion de codigo en un solo lugar
- Aislamiento: se puede ejecutar sin exponer la implementacion

Un stored procedure puede aceptar una serie de parametros que luego seran utilizados en el script como si fueran variables.

## Ejecucion de un stored procedure

Sintaxis basica:

```sql
EXECUTE <nombre_del_stored_procedure> <parametros>
```

**Ej:** Agregar la conferencia `Pacific` utilizando `add_conference`

```sql
EXECUTE add_conference 'Pacific'
```

Los parametros pueden ser provistos en el orden en el que el stored procedure los declara, o pueden utilizarse parametros nombrados:

```sql
EXECUTE add_conference @conference_name = 'Pacific'
```

Algunos stored procedures devuelven datos (en el caso de `add_conference`, devuelve el id de la conferencia creada), otros simplemente ejecutan una serie de comandos.

# Creacion de un stored procedure

## Sintaxis basica

```sql
CREATE OR ALTER PROCEDURE <nombre_del_stored_procedure>  
    -- Parametros
AS   
    -- Comandos
GO  
```

Algunas observaciones:

- `CREATE OR ALTER` implica que el script que crea el stored procedure va a crearlo si no existe, o actualizarlo si ya existe. Muy util para no tener que mantener dos scripts de creacion y actualizacion, o utilizar logica innecesariamente.
- `GO` fuerza todo el bloque a ejecutarse en una sola transaccion.

## Parametros

Los parametros se declaran exactamente igual que una variable. El fragmento del ejemplo anterior se completaria de la siguiente forma:

```sql
CREATE OR ALTER PROCEDURE [dbo].[add_team]
(
	@name NVARCHAR(1000),
    @conference_id INT,
	@country_id INT
)
```

En caso de tener parametros que acepten valores nulos (es decir, parametros opcionales), se les asigna un valor por defecto:

```sql
CREATE OR ALTER PROCEDURE [dbo].[add_team]
(
	@name NVARCHAR(1000),
    @conference_id INT = NULL,
	@country_id INT = NULL
)
```

## Return

En el caso de querer devolver un valor (usualmente conocido como `return`), si bien hay muchas formas de lograrlo, la mas facil de todas es utilizando un `SELECT`.

**Ej:** Seccion `SELECT` de un stored procedure que devuelve datos de un miembro si se le provee un `@member_id`:

```sql
SELECT
    m.first_name 'Name',
    m.last_name 'Surname',
    m.supported_team_id 'Supported team'
FROM member m
JOIN team t ON m.supported_team_id = t.team_id
WHERE m.member_id = @member_id;
```

En caso de haber insertado un registro, es posible acceder a la `primary_key` que resulto de la transaccion a traves de `@@IDENTITY`:

```sql
-- Return created entity
SELECT @@IDENTITY 'Team id';
```

# Homework

Crear stored procedures para operaciones CRUD en la tabla `team`.

**Importante:** Agregar el nombre del alumno al final del stored procedure para poder ejecutarlos a todos en la misma base de datos sin conflictos!

### 1. get_all_teams_<alumno>

Devuelve todos los equipos, incluyendo:

- Nombre del equipo
- Nombre del pais al que pertenece el equipo
- Nombre de la conferencia en la que juega el equipo

### 2. get_team_<alumno>

Igual que el stored procedure anterior, pero devolviendo unicamente un equipo de acuerdo al parametro `@team_id`.

### 3. update_team_<alumno>

Actualiza los datos de un equipo:

- Nombre del equipo
- Id del pais al que pertenece el equipo
- Id de la conferencia en la que juega el equipo

### 4. add_team_<alumno>

Agrega un equipo, utilizando algunos parametros opcionales:

- Nombre del equipo
- Id del pais al que pertenece el equipo (opcional)
- Id de la conferencia en la que juega el equipo (opcional)

### 5. delete_team_<alumno>

Borra un equipo de la tabla `team`, y a su vez actualiza la tabla `member` para que sus hinchas pasen a no tener equipo.

### 6. update_team_v2<alumno>

Escribir una version de `update_team_<alumno>`, pero esta vez los parametros que recibe el stored procedure son:

- `@team_name`
- `@country_name`
- `@conference_name`