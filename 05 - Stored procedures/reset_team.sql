-- DESCRIPTION:
-- Creates stored procedure [dbo].[reset_team]

CREATE OR ALTER PROCEDURE [dbo].[reset_team]
AS
	SET NOCOUNT ON;
	
    DELETE team;

	INSERT INTO dbo.team([name], [conference_id], [country_id])
    VALUES
        ('Brumbies',1,9),
        ('Rebels',1,9),
        ('Reds',1,9),
        ('Sunwolves',1,85),
        ('Waratahs',1,9),
        ('Blues',2,127),
        ('Chiefs',2,127),
        ('Crusaders',2,127),
        ('Highlanders',2,127),
        ('Hurricanes',2,127),
        ('Bulls',3,162),
        ('Jaguares',3,7),
        ('Lions',3,162),
        ('Sharks',3,162),
        ('Stormers',3,162);
		
	SET NOCOUNT OFF;
GO

-- End of the script
