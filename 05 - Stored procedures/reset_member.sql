-- DESCRIPTION:
-- Creates stored procedure [dbo].[reset_member]

CREATE OR ALTER PROCEDURE [dbo].[reset_member]
AS
	SET NOCOUNT ON;
	
    DELETE member;

	INSERT INTO dbo.member(
        [first_name],
        [last_name],
        [date_of_birth],
        [nationality_id],
        [supported_team_id])
    VALUES
        ('Jonatan', 'Jaworski', '1987-09-29', 1, 12),
        ('Mirtha', 'Legrand', '1205-07-15', 2, 13),
        ('Sergio', 'Denis', '1962-02-01', 1, 7),
        ('Teto', 'Medina', '1962-05-27', 3, 7),
        ('Roberto', 'Carlos', '1955-03-17', 2, 1),
        ('Jamiro', 'Quai', '1990-11-20', 10, 3),
        ('Azucar', 'Amargo', '1975-01-30', 50, 10),
        ('Topo', 'Yiyo', '1962-12-14', 57, 2),
        ('Juan Carlos', 'Batman', '1960-04-2', 57, 3),
        ('Juan Domingo', 'Peron', '1974-03-1', 42, 13),
        ('Pepe', 'Biondi', '1957-07-14', 42, 11),
        ('Noam', 'Chomsky', '1995-04-17', 100, 8),
        ('Horacio', 'Guarani', '1987-12-02', 109, 10),
        ('Silvio', 'Soldan', '1992-10-01', 25, 8),
        ('Susana', 'Gimenez', '1989-01-23', 55, 12),
        ('Valeria', 'Lynch', '1975-02-01', 36, 8),
        ('Susan', 'Sarandonga', '1982-12-12', 1, 8),
        ('Joseph', 'Stalin', '1977-06-09', 99, NULL);
		
	SET NOCOUNT OFF;
GO

-- End of the script
