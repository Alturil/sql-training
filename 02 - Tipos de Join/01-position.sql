DROP TABLE position;

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[position](
	[position_id] [int] IDENTITY(1,1) NOT NULL,
    [number] [int] NOT NULL,
	[position] [nvarchar](1000) NOT NULL,
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[position] ADD  CONSTRAINT [PK_position] PRIMARY KEY CLUSTERED 
(    
	[position_id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY]
GO


INSERT INTO dbo.position([position], [number])
VALUES
('Full back', 15),
('Right wing', 14),
('Outside centre', 13),
('Inside centre', 12),
('Left wing', 11),
('Fly-half', 10),
('Scrum-half',	9	),
('Loosehead prop', 1),
('Hooker', 2),
('Tighthead prop', 3),
('Lock', 4),
('Lock', 5),
('Blindside flanker', 6),
('Openside flanker', 7),
('Number eight', 8	);