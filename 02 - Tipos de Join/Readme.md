# 02. Tipos de JOIN

## 1. Aliases

Se pueden asignar aliases a distintos bloques para hacer las consultas mas legibles.

**Ej:** Aliases para las columnas `first_name` y `last_name`:

```sql
SELECT first_name 'Name', last_name 'Surname'
FROM member;
```

Esto es especialmente util para los bloques `JOIN`, porque nos ahorran escribir todo el nombre de la tabla.

**Ej:** Aliases para las tablas `member` y `team`:

```sql
SELECT m.first_name, m.last_name, c.country
FROM member m
JOIN country c
ON m.nationality_id = c.country_id;
```

Un poco mas elegante todavia:

```sql
SELECT
    m.first_name 'Name',
    m.last_name 'Surname',
    c.country 'Country'
FROM member m
JOIN country c
ON m.nationality_id = c.country_id;
```

## 2. Multiples JOIN en la misma consulta

**Ej:** Seleccionar nombre, apellido, nombre del equipo del que son hinchas y nombre del pais de cada miembro:

```sql
SELECT
    m.first_name 'Name',
    m.last_name 'Surname',
    t.name 'Supported team',
    c.country 'Nationality'
FROM member m
JOIN team t ON m.supported_team_id = t.team_id
JOIN country c ON m.nationality_id = c.country_id;
```

## 3. NULL

Una fila que contiene un valor `NULL` para cierta columna indica que el valor es _indefinido_.

Se puede utilizar en filtros indicando `IS NULL` o `IS NOT NULL`:

**Ej:** Miembros que son hinchas de algun equipo:

```sql
SELECT
    first_name 'Name',
    last_name 'Surname',
    supported_team_id 'Supported team id'
FROM member
WHERE supported_team_id IS NOT NULL
```

**Ej:** Miembros que NO son hinchas de ningun equipo:

```sql
SELECT
    first_name 'Name',
    last_name 'Surname',
    supported_team_id 'Supported team id'
FROM member
WHERE supported_team_id IS NULL
```

## 4. Distintos tipos de JOIN

El modificador que precede a `JOIN` determina que segmento se selecciona durante la query:

![joins](https://miro.medium.com/max/914/1*3ZU-U1xkeXobAKSy_WyWww.png "Diferentes joins")

El modificador por defecto es `INNER`.

**Ej:** Seleccionar nombre, apellido y nombre del equipo del que son hinchas usando `INNER JOIN`:

```sql
SELECT m.first_name 'Name', m.last_name 'Surname', t.name 'Team'
FROM member m
JOIN team t ON m.supported_team_id = t.team_id;
```
Esta query exclude miembros que no son hinchas de ningun equipo, y equipos sin hinchas.

**Ej:** Seleccionar nombre, apellido y nombre del equipo del que son hinchas usando `LEFT JOIN`:

```sql
SELECT m.first_name 'Name', m.last_name 'Surname', t.name 'Team'
FROM member m
LEFT JOIN team t ON m.supported_team_id = t.team_id
ORDER BY t.name;
```
Esta query incluye miembros que no son hinchas de ningun equipo, y excluye equipos sin hinchas.

**Ej:** Seleccionar nombre, apellido y nombre del equipo del que son hinchas usando `RIGHT JOIN`:

```sql
SELECT m.first_name 'Name', m.last_name 'Surname', t.name 'Team'
FROM member m
RIGHT JOIN team t ON m.supported_team_id = t.team_id
ORDER BY m.first_name;
```
Esta query excluye miembros que no son hinchas de ningun equipo, e incluye equipos sin hinchas.

**Ej:** Seleccionar nombre, apellido y nombre del equipo del que son hinchas usando `FULL JOIN`:

```sql
SELECT m.first_name 'Name', m.last_name 'Surname', t.name 'Team'
FROM member m
FULL JOIN team t ON m.supported_team_id = t.team_id;
```
Esta query incluye miembros que no son hinchas de ningun equipo, y equipos sin hinchas.

# Homework

1. Seleccionar todos los miembros y sus respectivas nacionalidades, incluyendo paises sin miembros asociados.
2. Seleccionar equipos de Super Rugby y sus respectivos paises, excluyendo los que paises que no tengan equipos.
3. Seleccionar todos los miembros indicando nombre, apellido, nombre del equipo del que son hinchas, y pais al que pertenece el equipo, incluyendo miembros que no son hinchas de nadie.