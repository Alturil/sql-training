DROP TABLE player;

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[player](
	[player_id] [int] IDENTITY(1,1) NOT NULL,
	[first_name] [nvarchar](1000) NOT NULL,
    [last_name] [nvarchar](1000) NOT NULL,
    [position_id] [int] NULL,
	[team_id] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[player] ADD  CONSTRAINT [PK_player] PRIMARY KEY CLUSTERED 
(
	[player_id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY]
GO


INSERT INTO dbo.player([first_name], [last_name], [position_id], [team_id])
VALUES
-- Brumbies
('Allan','Alaalatoa',10,1),
('Sio','Scott',1,1),
('Kuridrani','Kuridrani',3,1),
('Toni','Pulu',2,1),
('James','Slipper',1,1),
-- Rebels
('','',,2),
('','',,2),
('','',,2),
('','',,2),
('','',,2),
-- Reds
('','',,3),
('','',,3),
('','',,3),
('','',,3),
('','',,3),
-- Sunwolves
('','',,)4,
('','',,)4,
('','',,)4,
('','',,)4,
('','',,)4,
-- Waratahs
('','',,5),
('','',,5),
('','',,5),
('','',,5),
('','',,5),
-- Blues
('','',,6),
('','',,6),
('','',,6),
('','',,6),
('','',,6),
-- Chiefs
('','',,7),
('','',,7),
('','',,7),
('','',,7),
('','',,7),
-- Crusaders
('','',,8),
('','',,8),
('','',,8),
('','',,8),
('','',,8),
-- Highlanders
('','',,9),
('','',,9),
('','',,9),
('','',,9),
('','',,9),
-- Hurricanes
('','',,10),
('','',,10),
('','',,10),
('','',,10),
('','',,10),
-- Bulls
('','',,11),
('','',,11),
('','',,11),
('','',,11),
('','',,11),
-- Jaguares
('','',,12),
('','',,12),
('','',,12),
('','',,12),
('','',,12),
-- Lions
('','',,13),
('','',,13),
('','',,13),
('','',,13),
('','',,13),
-- Sharks
('','',,14),
('','',,14),
('','',,14),
('','',,14),
('','',,14),
-- Stormers
('','',,15),
('','',,15),
('','',,15),
('','',,15),
('','',,15);