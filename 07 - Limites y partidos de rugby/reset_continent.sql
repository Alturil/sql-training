-- DESCRIPTION:
-- Creates stored procedure [dbo].[reset_continent]

-- CREATE OR ALTER PROCEDURE [dbo].[reset_continent]
ALTER PROCEDURE [dbo].[reset_continent]
AS
	SET NOCOUNT ON;
	
    IF OBJECT_ID('dbo.continent', 'U') IS NOT NULL       
        DROP TABLE [continent];

    CREATE TABLE [dbo].[continent](
        [continent_id] [int] IDENTITY(1,1) NOT NULL,
        [continent] [nvarchar](100) NOT NULL,
        [code_name] [nvarchar](2) NOT NULL
    ) ON [PRIMARY];    

    ALTER TABLE [dbo].[continent] ADD  CONSTRAINT [PK_continent] PRIMARY KEY CLUSTERED 
    (
        [continent_id] ASC
    )WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY];    
    
    INSERT INTO dbo.continent([continent], [code_name])
    VALUES
    ('Europe', 'EU'),
    ('Asia', 'AS'),
    ('North America', 'NA'),
    ('Africa', 'AF'),
    ('Antarctica', 'AN'),
    ('South America', 'SA'),
    ('Oceania', 'OC');    
		
	SET NOCOUNT OFF;
GO

-- End of the script