# 07 - Limites y partidos de rugby

## Limites

Muchas veces una consulta busca filtrar un rango a partir de una condicion sobre una columna en particular, para lo cual se utilizan operadores. Hay que prestar atencion al enunciado para identificar si el rango que se quiere devolver incluye o no incluye los limites.

Excluyentes:
- `>`: Mayor
- `<`: Menor

Incluyentes:
- `>=`: Mayor **o igual**
- `<=`: Menor **o igual**

Otra forma muy semantica es utilizando `BETWEEN ... AND`:

**Ej:** Seleccionar todos los miembros nacidos entre el 1 de Enero de 1985 y el 2 de Septiembre de 1995:

```sql
SELECT
    first_name,
    last_name,
    date_of_birth
FROM member
WHERE date_of_birth >= '1985-01-01'
  AND date_of_birth <= '1995-02-01';
```

Utilizando `BETWEEN ... AND`:

```sql
SELECT
    first_name,
    last_name,
    date_of_birth
FROM member
WHERE date_of_birth BETWEEN '1985-01-01' AND '1995-02-01';
```

Es importante notar que el `AND` pertenece a la estructura del `BETWEEN`. El siguiente ejemplo tiene aparted filtrar por edad, descarta los miembros con apellido "Jaworski":

```sql
SELECT
    first_name,
    last_name,
    date_of_birth
FROM member
WHERE date_of_birth BETWEEN '1985-01-01' AND '1995-02-01'
AND last_name <> 'Jaworski';
```

# Novedades en la base de datos

Para practicar limites y profundizar en joins, la base de datos crecio para incorporar un par de cosas:

### Mas informacion sobre paises

La tabla `country` ahora tiene las capitales de cada pais, la poblacion, el area, y el continente al que pertenece.

La informacion sobre continentes esta en otra tabla llamada `continent`.

### Partidos de rugby

Hay una tabla nueva llamada `match` que tiene los resultados de los partidos del torneo de Super Rugby.

La tabla tiene el numero de ronda, el equipo local, el visitante y el resultado.

### Diagrama

El diagrama esta actualizado, y lo pueden encontrar siguiendo [este link](https://dbdiagram.io/d/5e01598cedf08a25543f5e49).

# Homework

Ejercicios faciles:

- Seleccionar los paises con un area de al menos 14000000 km2.
- Seleccionar los paises con una poblacion de entre 30 y 140 personas.
- Cuantos paises tiene cada continente?
- Seleccionar la capital del pais al que pertenece cada uno de los equipos.
- Cual es la poblacion total del continente con menor cantidad de personas?
- Que continente tiene la mayor cantidad de equipos de Super Rugby?
- Seleccionar el nombre de los equipos y el resultado de los partidos de la tercera ronda de Super Rugby.

Un poco mas dificil:

- En que ronda se hicieron mas puntos en total?
- Cual es el equipo que mas partidos gano como local?