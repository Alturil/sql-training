CREATE TABLE country(
   [country_id] INT IDENTITY(1,1) NOT NULL
  ,[country] NVARCHAR(100) NOT NULL
  ,[capital] NVARCHAR(100)
  ,[area]   INTEGER  NOT NULL
  ,[population] INTEGER  NOT NULL
  ,[continent_id] INTEGER  NOT NULL
);

INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Andorra','Andorra la Vella',468,84000,1);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('United Arab Emirates','Abu Dhabi',82880,4975593,2);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Afghanistan','Kabul',647500,29121286,2);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Antigua and Barbuda','St. John''s',443,86754,3);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Anguilla','The Valley',102,13254,3);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Albania','Tirana',28748,2986952,1);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Armenia','Yerevan',29800,2968000,2);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Netherlands Antilles','Willemstad',960,300000,3);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Angola','Luanda',1246700,13068161,4);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Antarctica',NULL,14000000,0,5);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Argentina','Buenos Aires',2766890,41343201,6);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('American Samoa','Pago Pago',199,57881,7);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Austria','Vienna',83858,8205000,1);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Australia','Canberra',7686850,21515754,7);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Aruba','Oranjestad',193,71566,3);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Åland','Mariehamn',1580,26711,1);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Azerbaijan','Baku',86600,8303512,2);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Bosnia and Herzegovina','Sarajevo',51129,4590000,1);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Barbados','Bridgetown',431,285653,3);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Bangladesh','Dhaka',144000,156118464,2);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Belgium','Brussels',30510,10403000,1);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Burkina Faso','Ouagadougou',274200,16241811,4);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Bulgaria','Sofia',110910,7000039,1);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Bahrain','Manama',665,738004,2);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Burundi','Gitega',27830,9863117,4);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Benin','Porto-Novo',112620,9056010,4);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Saint Barthélemy','Gustavia',21,8450,3);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Bermuda','Hamilton',53,65365,3);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Brunei','Bandar Seri Begawan',5770,395027,2);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Bolivia','Sucre',1098580,9947418,6);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Bonaire, Sint Eustatius, and Saba',NULL,328,18012,3);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Brazil','Brasilia',8511965,201103330,6);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Bahamas','Nassau',13940,301790,3);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Bhutan','Thimphu',47000,699847,2);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Bouvet Island',NULL,49,0,5);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Botswana','Gaborone',600370,2029307,4);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Belarus','Minsk',207600,9685000,1);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Belize','Belmopan',22966,314522,3);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Canada','Ottawa',9984670,33679000,3);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Cocos [Keeling] Islands','West Island',14,628,2);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('DR Congo','Kinshasa',2345410,70916439,4);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Central African Republic','Bangui',622984,4844927,4);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Congo Republic','Brazzaville',342000,3039126,4);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Switzerland','Bern',41290,8484100,1);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Ivory Coast','Yamoussoukro',322460,21058798,4);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Cook Islands','Avarua',240,21388,7);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Chile','Santiago',756950,16746491,6);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Cameroon','Yaounde',475440,19294149,4);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('China','Beijing',9596960,1330044000,2);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Colombia','Bogota',1138910,47790000,6);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Costa Rica','San Jose',51100,4516220,3);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Serbia and Montenegro','Belgrade',102350,10829175,1);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Cuba','Havana',110860,11423000,3);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Cabo Verde','Praia',4033,508659,4);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Curaçao','Willemstad',444,141766,3);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Christmas Island','Flying Fish Cove',135,1500,7);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Cyprus','Nicosia',9250,1102677,1);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Czechia','Prague',78866,10476000,1);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Germany','Berlin',357021,81802257,1);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Djibouti','Djibouti',23000,740528,4);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Denmark','Copenhagen',43094,5484000,1);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Dominica','Roseau',754,72813,3);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Dominican Republic','Santo Domingo',48730,9823821,3);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Algeria','Algiers',2381740,34586184,4);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Ecuador','Quito',283560,14790608,6);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Estonia','Tallinn',45226,1291170,1);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Egypt','Cairo',1001450,80471869,4);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Western Sahara','El-Aaiun',266000,273008,4);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Eritrea','Asmara',121320,5792984,4);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Spain','Madrid',504782,46505963,1);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Ethiopia','Addis Ababa',1127127,88013491,4);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Finland','Helsinki',337030,5244000,1);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Fiji','Suva',18270,875983,7);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Falkland Islands','Stanley',12173,2638,6);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Micronesia','Palikir',702,107708,7);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Faroe Islands','Torshavn',1399,48228,1);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('France','Paris',547030,64768389,1);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Gabon','Libreville',267667,1545255,4);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('United Kingdom','London',244820,62348447,1);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Grenada','St. George''s',344,107818,3);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Georgia','Tbilisi',69700,4630000,2);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('French Guiana','Cayenne',91000,195506,6);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Guernsey','St Peter Port',78,65228,1);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Ghana','Accra',239460,24339838,4);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Gibraltar','Gibraltar',7,27884,1);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Greenland','Nuuk',2166086,56375,3);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Gambia','Banjul',11300,1593256,4);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Guinea','Conakry',245857,10324025,4);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Guadeloupe','Basse-Terre',1780,443000,3);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Equatorial Guinea','Malabo',28051,1014999,4);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Greece','Athens',131940,11000000,1);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('South Georgia and South Sandwich Islands','Grytviken',3903,30,5);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Guatemala','Guatemala City',108890,13550440,3);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Guam','Hagatna',549,159358,7);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Guinea-Bissau','Bissau',36120,1565126,4);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Guyana','Georgetown',214970,748486,6);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Hong Kong','Hong Kong',1092,6898686,2);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Heard Island and McDonald Islands',NULL,412,0,5);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Honduras','Tegucigalpa',112090,7989415,3);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Croatia','Zagreb',56542,4284889,1);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Haiti','Port-au-Prince',27750,9648924,3);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Hungary','Budapest',93030,9982000,1);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Indonesia','Jakarta',1919440,242968342,2);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Ireland','Dublin',70280,4622917,1);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Israel','Jerusalem',20770,7353985,2);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Isle of Man','Douglas',572,75049,1);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('India','New Delhi',3287590,1173108018,2);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('British Indian Ocean Territory','Diego Garcia',60,4000,2);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Iraq','Baghdad',437072,29671605,2);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Iran','Tehran',1648000,76923300,2);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Iceland','Reykjavik',103000,308910,1);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Italy','Rome',301230,60340328,1);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Jersey','Saint Helier',116,90812,1);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Jamaica','Kingston',10991,2847232,3);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Jordan','Amman',92300,6407085,2);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Japan','Tokyo',377835,127288000,2);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Kenya','Nairobi',582650,40046566,4);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Kyrgyzstan','Bishkek',198500,5776500,2);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Cambodia','Phnom Penh',181040,14453680,2);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Kiribati','Tarawa',811,92533,7);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Comoros','Moroni',2170,773407,4);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('St Kitts and Nevis','Basseterre',261,51134,3);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('North Korea','Pyongyang',120540,22912177,2);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('South Korea','Seoul',98480,48422644,2);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Kuwait','Kuwait City',17820,2789132,2);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Cayman Islands','George Town',262,44270,3);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Kazakhstan','Nur-Sultan',2717300,15340000,2);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Laos','Vientiane',236800,6368162,2);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Lebanon','Beirut',10400,4125247,2);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Saint Lucia','Castries',616,160922,3);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Liechtenstein','Vaduz',160,35000,1);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Sri Lanka','Colombo',65610,21513990,2);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Liberia','Monrovia',111370,3685076,4);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Lesotho','Maseru',30355,1919552,4);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Lithuania','Vilnius',65200,2944459,1);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Luxembourg','Luxembourg',2586,497538,1);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Latvia','Riga',64589,2217969,1);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Libya','Tripoli',1759540,6461454,4);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Morocco','Rabat',446550,33848242,4);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Monaco','Monaco',2,32965,1);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Moldova','Chisinau',33843,4324000,1);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Montenegro','Podgorica',14026,666730,1);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Saint Martin','Marigot',53,35925,3);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Madagascar','Antananarivo',587040,21281844,4);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Marshall Islands','Majuro',181,65859,7);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('North Macedonia','Skopje',25333,2062294,1);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Mali','Bamako',1240000,13796354,4);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Myanmar','Nay Pyi Taw',678500,53414374,2);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Mongolia','Ulaanbaatar',1565000,3086918,2);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Macao','Macao',254,449198,2);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Northern Mariana Islands','Saipan',477,53883,7);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Martinique','Fort-de-France',1100,432900,3);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Mauritania','Nouakchott',1030700,3205060,4);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Montserrat','Plymouth',102,9341,3);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Malta','Valletta',316,403000,1);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Mauritius','Port Louis',2040,1294104,4);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Maldives','Male',300,395650,2);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Malawi','Lilongwe',118480,17563749,4);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Mexico','Mexico City',1972550,112468855,3);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Malaysia','Kuala Lumpur',329750,28274729,2);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Mozambique','Maputo',801590,22061451,4);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Namibia','Windhoek',825418,2128471,4);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('New Caledonia','Noumea',19060,216494,7);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Niger','Niamey',1267000,15878271,4);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Norfolk Island','Kingston',35,1828,7);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Nigeria','Abuja',923768,154000000,4);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Nicaragua','Managua',129494,5995928,3);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Netherlands','Amsterdam',41526,16645000,1);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Norway','Oslo',324220,5009150,1);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Nepal','Kathmandu',140800,28951852,2);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Nauru','Yaren',21,10065,7);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Niue','Alofi',260,2166,7);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('New Zealand','Wellington',268680,4252277,7);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Oman','Muscat',212460,2967717,2);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Panama','Panama City',78200,3410676,3);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Peru','Lima',1285220,29907003,6);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('French Polynesia','Papeete',4167,270485,7);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Papua New Guinea','Port Moresby',462840,6064515,7);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Philippines','Manila',300000,99900177,2);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Pakistan','Islamabad',803940,184404791,2);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Poland','Warsaw',312685,38500000,1);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Saint Pierre and Miquelon','Saint-Pierre',242,7012,3);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Pitcairn Islands','Adamstown',47,46,7);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Puerto Rico','San Juan',9104,3916632,3);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Palestine','East Jerusalem',5970,3800000,2);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Portugal','Lisbon',92391,10676000,1);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Palau','Melekeok',458,19907,7);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Paraguay','Asuncion',406750,6375830,6);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Qatar','Doha',11437,840926,2);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Réunion','Saint-Denis',2517,776948,4);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Romania','Bucharest',237500,21959278,1);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Serbia','Belgrade',88361,7344847,1);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Russia','Moscow',17100000,140702000,1);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Rwanda','Kigali',26338,11055976,4);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Saudi Arabia','Riyadh',1960582,25731776,2);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Solomon Islands','Honiara',28450,559198,7);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Seychelles','Victoria',455,88340,4);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Sudan','Khartoum',1861484,35000000,4);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Sweden','Stockholm',449964,9828655,1);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Singapore','Singapore',693,4701069,2);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Saint Helena','Jamestown',410,7460,4);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Slovenia','Ljubljana',20273,2007000,1);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Svalbard and Jan Mayen','Longyearbyen',62049,2550,1);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Slovakia','Bratislava',48845,5455000,1);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Sierra Leone','Freetown',71740,5245695,4);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('San Marino','San Marino',61,31477,1);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Senegal','Dakar',196190,12323252,4);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Somalia','Mogadishu',637657,10112453,4);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Suriname','Paramaribo',163270,492829,6);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('South Sudan','Juba',644329,8260490,4);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('São Tomé and Príncipe','Sao Tome',1001,197700,4);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('El Salvador','San Salvador',21040,6052064,3);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Sint Maarten','Philipsburg',21,37429,3);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Syria','Damascus',185180,22198110,2);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Eswatini','Mbabane',17363,1354051,4);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Turks and Caicos Islands','Cockburn Town',430,20556,3);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Chad','N''Djamena',1284000,10543464,4);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('French Southern Territories','Port-aux-Francais',7829,140,5);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Togo','Lome',56785,6587239,4);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Thailand','Bangkok',514000,67089500,2);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Tajikistan','Dushanbe',143100,7487489,2);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Tokelau',NULL,10,1466,7);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Timor-Leste','Dili',15007,1154625,7);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Turkmenistan','Ashgabat',488100,4940916,2);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Tunisia','Tunis',163610,10589025,4);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Tonga','Nuku''alofa',748,122580,7);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Turkey','Ankara',780580,77804122,2);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Trinidad and Tobago','Port of Spain',5128,1328019,3);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Tuvalu','Funafuti',26,10472,7);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Taiwan','Taipei',35980,22894384,2);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Tanzania','Dodoma',945087,41892895,4);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Ukraine','Kyiv',603700,45415596,1);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Uganda','Kampala',236040,33398682,4);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('U.S. Minor Outlying Islands',NULL,0,0,7);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('United States','Washington',9629091,310232863,3);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Uruguay','Montevideo',176220,3477000,6);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Uzbekistan','Tashkent',447400,27865738,2);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Vatican City','Vatican City',0,921,1);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('St Vincent and Grenadines','Kingstown',389,104217,3);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Venezuela','Caracas',912050,27223228,6);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('British Virgin Islands','Road Town',153,21730,3);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('U.S. Virgin Islands','Charlotte Amalie',352,108708,3);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Vietnam','Hanoi',329560,89571130,2);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Vanuatu','Port Vila',12200,221552,7);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Wallis and Futuna','Mata Utu',274,16025,7);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Samoa','Apia',2944,192001,7);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Kosovo','Pristina',10908,1800000,1);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Yemen','Sanaa',527970,23495361,2);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Mayotte','Mamoudzou',374,159042,4);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('South Africa','Pretoria',1219912,49000000,4);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Zambia','Lusaka',752614,13460305,4);
INSERT INTO country(country,capital,area,population,continent_id) VALUES ('Zimbabwe','Harare',390580,13061000,4);
