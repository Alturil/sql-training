# Attibute and Skill

Vamos a ampliar la base de datos agregando `attribute` y `skill`. Lo vamos a hacer en dos partes, asi que aca va la primera entrega con `attribute`.

## Attribute

Los atributos son cualidades basicas que tiene un personaje. Vamos a definir 6 (3 fisicas y 3 mentales), que estan en la tabla `attribute`.

Al igual que con los niveles, los personajes van a ir incrementando el valor de sus atributos, y como resultado van a tener un modificador a todas las habilidades relacionadas con dicho atributo. Esta progresion esta definida en `attribute_score`.

Por ultimo, cada personaje tiene sus seis atributos con el valor actual definido en la tabla `character_attribute`. Esta tabla no tiene datos por el momento, asi que agreguen y modifiquen a gusto, voy a tratar de agregar informacion existente hoy para la mayoria de los personajes.

# Homework

El unico punto relacionado a las entidades nuevas de la base de datos es el primero, los otros dos son ejercicios basados en lo anterior.

1. Actualizar el diagrama de base de datos con las tablas nuevas
2. Crear un script (opcionalmente convertirlo en un Stored Procedure) que tenga como valor de entrada el id de un personaje, y que devuelva como resultado una tabla con elementos de `equipment`, consignando el id, nombre del tipo de equipamiento, nombre del item, costo y peso, agrupados por tipo y ordenados alfabeticamente dentro de cada grupo. Unicamente devolver los items cuyo peso sumado al peso que lleva el personaje en ese momento no exceda el maximo que tiene por raza.
3. Diseñar tests cortitos que comprueben el script anterior.

# Homework pendiente de la clase anterior

El archivo `Homework 15.md` tiene la resolucion del ejercicio que faltaba. Cada uno deberia agregarlo a sus respectivos branches de tara de la clase 15 y completar el merge request.