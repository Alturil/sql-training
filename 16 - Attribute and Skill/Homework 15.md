# Homework pendiente clase 15 (#4)

Crear una funcion de conversion
que tome como parametros de entrada:
- el valor original
- el id de la moneda original
- id de la moneda de destino.

Como resultado, tiene que devolver el valor en la moneda de destino.

Ej: 5 sp to  => 50 pc.
- Valor: 5
- Moneda original: sp = 2
- Moneda destino: pc = 1
- Resultado: 50

```sql
CREATE OR ALTER FUNCTION [dbo].[get_converted_coin_amount_alturil]
(
    @amount DECIMAL(10,2),
    @from_coin_id INT,
    @to_coin_id INT   
)
RETURNS DECIMAL(10,2)
BEGIN
    DECLARE @from_value_in_cp DECIMAL(10,2) =
    (
        SELECT value_in_cp FROM coin WHERE coin_id = @from_coin_id
    );

    DECLARE @to_value_in_cp DECIMAL(10,2) =
    (
        SELECT value_in_cp FROM coin WHERE coin_id = @to_coin_id
    );

    DECLARE @result DECIMAL(10,2) =
    (
        @amount * @from_value_in_cp / @to_value_in_cp
    );

    RETURN @result;
END
GO
```