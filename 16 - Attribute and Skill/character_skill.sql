DROP TABLE IF EXISTS [character_skill];

CREATE TABLE [character_skill](
        [character_skill_id] INT IDENTITY(1,1) NOT NULL,
        [character_id] INT NOT NULL,
        [skill_id] INT NOT NULL,
        [score] INT NOT NULL
    ) ON [PRIMARY];
    
ALTER TABLE [dbo].[character_skill] ADD  CONSTRAINT [PK_character_skill] PRIMARY KEY CLUSTERED 
    ([character_skill_id] ASC) WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY];

INSERT INTO [character_skill]
(
    character_id,
    skill_id,
    score
)
VALUES
(1,1,5),
(1,3,2),
(1,5,8),
(2,5,4),
(2,4,6),
(2,9,2);