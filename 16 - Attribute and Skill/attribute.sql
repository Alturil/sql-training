DROP TABLE IF EXISTS [attribute];

CREATE TABLE [attribute]
(
    [attribute_id] INT IDENTITY(1,1) NOT NULL,
    [attribute] NVARCHAR(1000) NOT NULL
);
    
ALTER TABLE [dbo].[attribute]
ADD CONSTRAINT [PK_attribute] PRIMARY KEY CLUSTERED
(
    [attribute_id] ASC
);

INSERT INTO [attribute] ([attribute])
VALUES
('strength'),
('dexterity'),
('constitution'),
('intelligence'),
('wisdom'),
('charisma');