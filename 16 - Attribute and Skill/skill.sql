DROP TABLE IF EXISTS [skill];

CREATE TABLE [skill](
        [skill_id] INT IDENTITY(1,1) NOT NULL,
        [skill] NVARCHAR(1000) NOT NULL,
        [attribute_id] INT NOT NULL
    ) ON [PRIMARY];
    
ALTER TABLE [dbo].[skill] ADD  CONSTRAINT [PK_skill] PRIMARY KEY CLUSTERED 
    ([skill_id] ASC) WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY];

INSERT INTO [skill] ([skill], [attribute_id])
VALUES
-- Strength
('Athletics', 1),
-- Dexterity
('Acrobatics', 2),
('Sleight of Hand', 2),
('Stealth', 2),
-- Intelligence
('Arcana', 4),
('History', 4),
('Investigation', 4),
('Nature', 4),
('Religion', 4),
-- Wisdom
('Animal Handling', 5),
('Insight', 5),
('Medicine', 5),
('Perception', 5),
('Survival', 5),
-- Charisma
('Deception', 6),
('Intimidation', 6),
('Performance', 6),
('Persuasion', 6);