DROP TABLE IF EXISTS [race_skill];

CREATE TABLE [race_skill](
        [race_skill_id] INT IDENTITY(1,1) NOT NULL,
        [race_id] INT NOT NULL,
        [skill_id] INT NOT NULL,
        [modifier] INT NOT NULL
    ) ON [PRIMARY];
    
ALTER TABLE [dbo].[race_skill] ADD  CONSTRAINT [PK_race_skill] PRIMARY KEY CLUSTERED 
    ([race_skill_id] ASC) WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY];

INSERT INTO [race_skill]
(
    race_id,
    skill_id,
    modifier
)
VALUES
(2,1,2),
(4,4,3),
(5,14,1),
(6,17,5);