DROP TABLE IF EXISTS [attribute_score];

CREATE TABLE [attribute_score](
        [attribute_score_id] INT IDENTITY(1,1) NOT NULL,
        [score] INT NOT NULL,
        [modifier] INT NOT NULL
    ) ON [PRIMARY];
    
ALTER TABLE [dbo].[attribute_score] ADD  CONSTRAINT [PK_attribute_score] PRIMARY KEY CLUSTERED 
    ([attribute_score_id] ASC) WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY];

INSERT INTO [attribute_score] ([score], [modifier])
VALUES
(1, -5),
(2, -4),
(4, -3),
(6, -2),
(8, -1),
(10, 0),
(12, 1),
(14, 2),
(16, 3),
(18, 4),
(20, 5),
(22, 6),
(24, 7),
(26, 8),
(28, 9),
(30, 10);