DROP TABLE IF EXISTS [character_attribute];

CREATE TABLE [character_attribute](
        [character_attribute_id] INT IDENTITY(1,1) NOT NULL,
        [character_id] INT NOT NULL,
        [attribute_id] INT NOT NULL,
        [score] INT NOT NULL
    ) ON [PRIMARY];
    
ALTER TABLE [dbo].[character_attribute] ADD  CONSTRAINT [PK_character_attribute] PRIMARY KEY CLUSTERED 
    ([character_attribute_id] ASC) WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY];