# UNION, Table Variable y Temporary Table

## `UNION`

`UNION` permite concatenar multiples resultados en uno, con lo cual es posible realizar consultas en un set que contiene resultados que responden a distintos criterios.

La sintaxis es sumamente simple: se utiliza la clausula `UNION` entre dos sets de resultados:

```sql
SELECT ... -- set 1

UNION

SELECT ... -- set 2
```

Los sets sepueden concatenar siempre y cuando:

- La cantidad y orden de las columanas es igual en todos los sets
- El tipo de datos de las columnas son compatibles

En caso de haber filas duplicadas, se puede especificar su exclusion utilizando `UNION`, o incluirlas utilizando `UNION ALL`.

El ejemplo que vimos en clase obtiene los resultados de cada equipo a lo largo del torneo. El primer set obtiene los resultados de todos los equipos locales, el segundo set el de los equipos visitantes, y luego se concatenan todos los resultados en un solo set:

```sql
SELECT
    m.[round],
    m.home_team_id 'team_id',
    m.visiting_team_id 'rival_team_id',
    m.home_team_score 'team_score',
    m.visiting_team_score 'rival_score',
    'home' 'condition'
FROM [match] m
JOIN team th ON m.home_team_id = th.team_id
JOIN team tv ON m.visiting_team_id = tv.team_id

    UNION

    SELECT
        m.[round],
        m.visiting_team_id 'team_id',
        m.home_team_id 'rival_team_id',
        m.visiting_team_score 'team_score',
        m.home_team_score 'rival_score',
        'visitor' 'condition'
    FROM [match] m
    JOIN team th ON m.home_team_id = th.team_id
    JOIN team tv ON m.visiting_team_id = tv.team_id;
```

Es importante destacar que no es posible aplicar operaciones como `ORDER BY` a uno de los resultados, sino al resultado total incluyendo la union de todas las consultas.

## Table Variable

Ocasionalmente necesitamos realizar multiples operaciones en base a un resultado obtenido, para lo cual `CTE` resulta insuficiente (ya que permite realizar unicamente una operacion). Una de las opciones disponibles es utilizar una `table variable`, es decir, una variable de tipo `TABLE`.

Sintacticamente se declara como una variable, y sus columnas se definen como en una tabla, consignando el nombre de cada una y su tipo de dato:

```sql
DECLARE @[nombre_de_table_variable]
    ([columna_1] [tipo_dato_col_1],
     ...,
     [columna_n] [tipo_dato_col_n]);
```

A la hora de realizar consultas, una table variable no presenta ninguna diferencia con una tabla convencional o un result set (puede ser posteriormente consultada, ordenada, agrupada, joineada, etc).

En el ejemplo que vimos en clase, creamos una table variable e insertamos en esa table variable el resultado de la query con estadisticas de los equipos en cada partido:

```sql

-- Declaramos la variable de tipo TABLE

DECLARE @resultados TABLE
(
    [round] INT,
    team_id INT,
    rival_team_id INT,
    team_score INT,
    rival_score INT,
    condition NVARCHAR(100)
);

-- Insertamos el resultado de la consulta del ejemplo anterior

INSERT INTO @resultados
SELECT
    m.[round],
    m.home_team_id 'team_id',
    m.visiting_team_id 'rival_team_id',
    m.home_team_score 'team_score',
    m.visiting_team_score 'rival_score',
    'home' 'condition'
FROM [match] m
JOIN team th ON m.home_team_id = th.team_id
JOIN team tv ON m.visiting_team_id = tv.team_id

    UNION

    SELECT
        m.[round],
        m.visiting_team_id 'team_id',
        m.home_team_id 'rival_team_id',
        m.visiting_team_score 'team_score',
        m.home_team_score 'rival_score',
        'visitor' 'condition'
    FROM [match] m
    JOIN team th ON m.home_team_id = th.team_id
    JOIN team tv ON m.visiting_team_id = tv.team_id;

-- Seleccionamos los resultados de la tabla

SELECT * FROM @resultados;
```

El scope de una table variable es una transaccion.

## Temporary Table

Una temporary table es una tabla que se crea programaticamente y tiene el scope de una sesion, es decir que no se destruye al finalizar una transaccion.

Al igual que las variables de tipo tabla, es posible utilizar una tabla temporaria como una tabla convencional o un resultado.

Se puede crear del mismo modo en que se crea una trabla convencional:

```sql
CREATE TABLE #[nombre_de_la_tabla]
(
    ([columna_1] [tipo_dato_col_1],
     ...,
     [columna_n] [tipo_dato_col_n]);
);
```

Pero tambien es posible insertar el resultado de una consulta directamente en una tabla temporar sin necesidad de escribir la defincion de la tabla utilizando la clausula `INTO`. Continuando con el ejemplo de la seccion anterior:

```sql
SELECT
    m.[round],
    m.home_team_id 'team_id',
    m.visiting_team_id 'rival_team_id',
    m.home_team_score 'team_score',
    m.visiting_team_score 'rival_score',
    'home' 'condition'
INTO #resultados
FROM [match] m
JOIN team th ON m.home_team_id = th.team_id
JOIN team tv ON m.visiting_team_id = tv.team_id

    UNION

    SELECT
        m.[round],
        m.visiting_team_id 'team_id',
        m.home_team_id 'rival_team_id',
        m.visiting_team_score 'team_score',
        m.home_team_score 'rival_score',
        'visitor' 'condition'
    FROM [match] m
    JOIN team th ON m.home_team_id = th.team_id
    JOIN team tv ON m.visiting_team_id = tv.team_id;

SELECT * FROM #resultados;
```

Es importante destacar que al menos que la sesion activa se desconecte, es necesario destruir la tabla manualmente utilizando `DROP TABLE`. Es una buena costumbre utilizar el modificador `IF EXISTS` para evitar errores en caso de que la tabla no exista:

```sql
DROP TABLE IF EXISTS #resultados;
```

Un error muy comun es intentar ejecutar un comando que crea una tabla temporaria mas de una vez, en cuyo caso es necesario utilizar el comando anterior.

# Homework

- Seleccionar la cantidad de partidos jugados como local de todos los equipos que no tengan hinchas, mas todos los equipos de la conferencia de Nueva Zelanda, sin incluir resultados duplicados.
- Cual es el pais que tiene mas equipos con un promedio de puntos anotados en todo el torneo por encima del promedio de todos los equipos?
- Consolidar un resultado que incluya el nombre del pais, el nombre del continente al que pertenece, area, poblacion y capital de los siguientes paises:
    - Los tres paises con mayor area
    - Los tres paises con menor poblacion
    - Todos los paises que no tengan capital