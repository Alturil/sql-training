# Level up!

Esta clase tiene dos temas:

- Modificar y borrar datos usando `JOIN`
- Introduccion a stored procedures

## `UPDATE` y `DELETE` usando `JOIN`

Se utiliza cuando hay que modificar o borrar filas en una tabla utilizando un criterio que se encuentre fuera de la tabla en cuestion. La sintaxis es ligeramente distinta, ya que se utiliza el alias de la tabla al inicio de la instruccion:

```sql
-- Update
DELETE [alias]
SET ...
FROM [tabla] [alias]
JOIN ...;

-- Delete
DELETE [alias]
FROM [tabla] [alias]
JOIN ...;
```

**Ej:** Quitarle el arma `Sword` a todos los personajes que esten usando ese arma:

```sql
DECLARE @weapon_name NVARCHAR(1000) = 'Sword';

DELETE cw
FROM [character_weapon] cw
JOIN weapon w ON cw.weapon_id = w.weapon_id
WHERE w.weapon = @weapon_name;
```

## Introduccion a Stored Procedures

Un stored procedure es simplemente una forma de encapsular un conjunto de comandos que pueden ser ejecutados con solo invocar el nombre del stored procedure.

### Creacion de un stored procedure

La sintaxis es bastante familiar una vez que nos acostumbramos a trabajar con variables; los datos con valores variables y explicitos se declaran como parametros, y los comandos pasan a ser la definicion del stored procedure:

```sql
CREATE PROCEDURE [nombre_del_stored_procedure]
    [parametro_1 tipo_dato_parametro_1],
    ...
    [parametro_n tipo_dato_parametro_n],
AS   
    [comandos];
GO 
```

El ejemplo de la seccion anterior puede ser transformado en un stored procedure por ejemplo:

```sql
CREATE PROCEDURE remove_weapon
(
    @weapon_name NVARCHAR(1000)
)
AS
    DELETE cw
    FROM [character_weapon] cw
    JOIN weapon w ON cw.weapon_id = w.weapon_id
    WHERE w.weapon = @weapon_name;
GO
```

Es considerado un tener alguna otra instruccion previa a la creacion de un stored procedure en la misma transaccion.

### Ejecucion de un stored procedure

Para ejecutar un stored procedure se utiliza la palabra `EXECUTE`, y se consignan los nombres de los parametros requeridos con sus respectivos valores:

```sql
EXECUTE [nombre_del_stored_procedure]
    [nombre_parametro_1] = [valor_parametro_1],
    ...
    [nombre_parametro_n] = [valor_parametro_n];
```

**Ej:** Ejecutamos el stored procedure del ejemplo anterior para quitarle el arma `Axe` a todos los personajes que la esten usando:

```sql
EXECUTE remove_weapon @weapon_name = 'Axe';
```

### Modificacion de un stored procedure

Para modificar un stored procedure, la sintaxis es exactamente la misma que para crear, excepto que el verbo a utilizar es `ALTER`.

Una expresion bastante comun es guardar la definicion de un stored procedure combinado `CREATE` y `ALTER`, para que en caso de ser la primera ejecucion cree el stored procedure, y en caso de ya existir lo modifique:

```sql
CREATE OR ALTER PROCEDURE remove_weapon
(
    @weapon_name NVARCHAR(1000)
)
AS
    DELETE cw
    FROM [character_weapon] cw
    JOIN weapon w ON cw.weapon_id = w.weapon_id
    WHERE w.weapon = @weapon_name;
GO
```

### Eliminacion de un stored procedure

Para eliminar un stored procedure simplemente se utiliza el verbo `DROP`:

```sql
DROP PROCEDURE [nombre_del_stored_procedure];
```

En nuestro caso:

```sql
DROP PROCEDURE remove_weapon;
```

Es posible tambien utilizar `IF EXISTS` para evitar intentar borrar el stored procedure si no existe:

```sql
DROP PROCEDURE IF EXISTS remove_weapon;
```

# Homework

**IMPORTANTE:** Vamos a crear una serie de stored procedures, y como vamos a compartir la misma base de datos, agreguen un sufijo unico. Por ejemplo, para el caso de `remove_weapon`, el mio podria ser `remove_weapon_jj` o similar.

Crear stored procedures para la siguiente funcionalidad:

- Traer nombre, nombre de raza, nombre de clase, edad y experiencia en un set, y una lista de las armas de ese personaje en otro set, teniendo como parametro el nombre del personaje.
- Tres stored procedures para agregar, borrar y modificar razas.
- Crear un personaje nuevo, teniendo como datos de entrada el nombre del personaje, el nombre de la clase, el nombre de la raza, la edad y la experiencia inicial.

**Bonus track:** Existe una tabla nueva llamada `level` que contiene la experiencia requerida para llegar a cierto nivel. La experiencia actual de cada personaje se puede ver en la columna `experience` de la tabla `character`.

Escribir dos stored procedures que hagan lo siguiente:

- Obtener el nivel actual de un personaje, teniendo como parametro de entrada el nombre del personaje.
- Hace "level up" de un personaje teniendo como parametro de entrada el nombre del personaje. En otras palabras, modificar su experiencia actual sumando la cantidad necesaria de puntos para alcanzar el siguiente nivel.