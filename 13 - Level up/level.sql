DROP TABLE IF EXISTS [level];

CREATE TABLE [level](
        [level] INT NOT NULL,
        [experience] INT NOT NULL
    ) ON [PRIMARY];
    
ALTER TABLE [dbo].[level] ADD  CONSTRAINT [PK_level] PRIMARY KEY CLUSTERED 
    ([level] ASC) WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY];

INSERT INTO [level] (level, experience)
VALUES
(1, 0),
(2, 300),
(3, 900),
(4, 2700),
(5, 6500),
(6, 14000),
(7, 23000),
(8, 34000),
(9, 48000),
(10, 64000),
(11, 85000),
(12, 100000),
(13, 120000),
(14, 140000),
(15, 165000),
(16, 195000),
(17, 225000),
(18, 265000),
(19, 305000),
(20, 355000);

/*
DECLARE @experience INT = 19000;

SELECT TOP 1 [level]
FROM [level]
WHERE experience <= @experience
ORDER BY 1 DESC;

SELECT * FROM [level];

*/