# Inventario

Se realizaron las siguientes adiciones a la base de datos:

### Tablas de equipamiento

Hay dos tablas nuevas:

`equipment_type` define los tipos de equipamiento disponible. Todos los items que un personaje puede tener estan clasificados de acuerdo a esta tabla.

`equipment` contiene una coleccion de items que un personaje puede poseer en su inventario. Cada item tiene un nombre, descripcion, costo y peso, mas el tipo de item de acuerdo a `equipment_type`.

Hay ciertos items que aparecen en esta tabla que a su vez poseen caracteristicas propias que hacen que esten en otras tablas. Por ejemplo, las armas estan en la tabla `weapon` a donde aparecen sus datos de daño, critico, etc, pero tambien estan en la tabla `equipment` a donde aparece su costo y su peso.

**Nota:** La tabla `weapon` todavia no tiene populados los datos en la columna de referencia a `equipment`, pero la relacion existe.

### Inventario

`inventory` define el inventario de los personajes, simplemente consignando en cada fila el id del personaje, el id del item, y la cantidad de dicho item.

Como en cualquier tabla intermedia, existe una fila por cada item distinto que el personaje posea.

La particularidad de esta tabla es que la columna `quantity` tiene el valor `1` por defecto, es decir que si al insertar no se provee un valor para esta columna, el valor en lugar de `NULL` va a ser `1`.

### Razas y capacidad de carga

Se agrega una columna a la tabla `race` que dice la cantidad maxima de libras que un personaje de dicha raza puede cargar en su inventario.

### Monedas

Se agrega la tabla `coin` que tiene la conversion del valor monetario de cada moneda, utilizando las monedas de cobre (cp) como denominador mas bajo.

A su vez, como un personaje puede tener monedas en su inventario, existe una referencia al item en la tabla `equipment`.

# Homework

1. Ampliar el esquema en https://dbdiagram.io/ incluyendo las tablas nuevas, columnas nuevas en tablas existentes a donde corresponda, y las relaciones.

2. Crear un stored procedure que permita agregar items al inventario de un personaje proveyendo el id de un personaje y el id del item. Si el personaje ya posee dicho item, incremental la cantidad en 1. Incluir gestion de errores a discrecion.

3. Crear una funcion que, provisto el id de un personaje, devuelva la cantidad de peso restante que tiene el personaje, considerando como limite el valor que tiene por raza, y como valor actual la suma de todos los pesos de los items que lleva en su inventario.

4. Crear una funcion de conversion que tome como parametros de entrada: el id de la moneda original, el valor original, y el id de la moneda de destino. Como resultado, tiene que devolver el valor en la moneda de destino. Ej: 5 pp => 50 pc.