DROP TABLE IF EXISTS [inventory];

CREATE TABLE [inventory](
        [inventory_id] INT IDENTITY(1,1) NOT NULL,
        [character_id] INT NOT NULL,
        [equipment_id] INT NOT NULL,
        [quantity] INT NULL DEFAULT 1    
    ) ON [PRIMARY];
    
ALTER TABLE [dbo].[inventory] ADD  CONSTRAINT [PK_inventory] PRIMARY KEY CLUSTERED 
    ([inventory_id] ASC) WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY];

INSERT INTO [inventory] ([character_id], [equipment_id])
VALUES
(1, 1),
(1, 30),
(1, 110),
(1, 55),
(1, 62),
(2, 35),
(2, 2),
(2, 40),
(2, 60),
(3, 22),
(3, 23),
(3, 99),
(3, 75),
(3, 88);