DROP TABLE IF EXISTS [coin];

CREATE TABLE [coin](
        [coin_id] INT IDENTITY(1,1) NOT NULL,
        [coin] NVARCHAR(1000) NOT NULL,
        [symbol] NVARCHAR(1000) NOT NULL,
        [value_in_cp] DECIMAL NOT NULL,
        [equipment_id] INT NULL        
    ) ON [PRIMARY];
    
ALTER TABLE [dbo].[coin] ADD  CONSTRAINT [PK_coin] PRIMARY KEY CLUSTERED 
    ([coin_id] ASC) WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY];

INSERT INTO [coin] ([coin], [symbol], [value_in_cp], [equipment_id])
VALUES
('Copper coin', 'cp', 1, 34),
('Silver coin', 'sp', 10, 94),
('Electrum coin', 'ep', 50, 48),
('Gold coin', 'gp', 100, 58),
('Platinum coin', 'pp', 1000, 87);