DROP TABLE IF EXISTS [equipment_type];

CREATE TABLE [equipment_type](
        [equipment_type_id] INT IDENTITY(1,1) NOT NULL,
        [equipment_type] NVARCHAR(1000) NOT NULL,
        [description] NVARCHAR(1000) NULL
    ) ON [PRIMARY];
    
ALTER TABLE [dbo].[equipment_type] ADD  CONSTRAINT [PK_equipment_type] PRIMARY KEY CLUSTERED 
    ([equipment_type_id] ASC) WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY];

INSERT INTO [equipment_type] ([equipment_type])
VALUES
('Adventuring gear'),
('Ammunition'),
('Armor'),
('Gemstone'),
('Holy Symbol'),
('Mount'),
('Potion'),
('Poison'),
('Tool'),
('Vehicle'),
('Weapon'),
('Coin');