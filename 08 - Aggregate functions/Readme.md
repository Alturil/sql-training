# 08 - Aggregate functions

Una funcion es un comportamiento predefinido que recibe una serie de parametros y devuelve un valor como resultado. Las funciones _aggregate_ se aplican sobre un grupo y devuelven un valor unico.

Una funcion ocupa el mismo rol que una columna o un valor (es decir, puede utilizarse en statements como `SELECT`, `ORDER BY`, etc.), y la sintaxis es simplemente:

```sql
<FUNCION> (<DISTINCT|ALL> <parametros>);
```

**Ej:** Funcion `COUNT`:

```sql
SELECT COUNT(member_id)
FROM member;
```

Las funciones mas comunes son: `COUNT`, `SUM`, `MIN`, `MAX` y `AVG`.

Todas las funciones de grupo pueden ser modificados por `DISTINCT` y `ALL`, que indica si la funcion debe utilizar unicamente valores unicos, o todos los valores respectivamente. Por defecto, las funciones operan asumiendo el modificador `ALL`.

## `COUNT`

Devuelve el conteo de la cantidad de elementos en un grupo:

**Ej:** Cantidad de equipos con hinchas:

```sql
SELECT COUNT(DISTINCT supported_team_id)
FROM member;
```

## `SUM`

Devuelve la suma de los valores en el grupo:

**Ej:** Poblacion mundial total:

```sql
SELECT SUM(population)
FROM country;
```

## `MIN`

Devuelve el minimo valor en el grupo:

**Ej:** Pais con la superficie mas chica:

```sql
SELECT MIN(area)
FROM country;
```

## `MAX`

Devuelve el maximo valor en el grupo:

**Ej:** Miembro mas joven del grupo:

```sql
SELECT MAX(date_of_birth)
FROM member;
```

## `AVG`

Devuelve el valor promedio del grupo:

**Ej:** Cantidad de :

```sql
SELECT MAX(date_of_birth)
FROM member;
```

## Operaciones matematicas

Tambien es posible aplicar operaciones matematicas utilizando las expresiones:

- `+` : suma
- `-` : resta
- `*` : multiplicacion
- `/` : division
- `%` : modulo

**Ej:** Calcular el precio total de una compra conociendo el precio unitario de los productos y su cantidad:

```sql
SELECT precio_unitario * cantidad 'Precio total';
```

Y utilizar funciones sobre valores unicos como `ABS`, que devuelve el valor absoluto:

```sql
SELECT ABS(-2), ABS(2); -- Las dos columnas devuelven '2'
```
# Homework

- Cuantos paises tiene cada continente?
- Cual es el maximo puntaje que un equipo anoto como visitante?
- Cuantos puntos se hicieron en total en la tercera ronda de Super Rugby?
- Calcular la diferencia de puntos de cada partido de Super Rugby (seleccionar numero de ronda, nombre y puntaje del equipo local, nombre y puntaje del equipo visitante, y diferencia de puntos).
- Cual es el promedio de poblacion por pais en cada continente?
- Cuantos hinchas tiene el equipo que menos puntos hizo como local en todo el torneo?
- Cuantos continentes hay incluidos en el torneo de Super Rugby?