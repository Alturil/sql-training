# Partition

Este comando se utiliza para identificar filas individuales en un set particionado, por lo cual resulta muy util a la hora de samplear datos o rankear informacion.

La operacion basicamente establece un numero de orden en cada fila del resultset dentro de cada particion, que posteriormente puede utilizarse para filtrar los resultados.

**Ej:** Devolver el item mas caro de cada categoria en la tabla `equipment`:

```sql
WITH ranked AS
(
    SELECT
        et.equipment_type,
        e.equipment,
        e.cost_gp,
        ROW_NUMBER() OVER
        (
            PARTITION BY et.equipment_type
            ORDER BY e.cost_gp DESC
        ) 'rank'
    FROM equipment e
    JOIN equipment_type et ON e.equipment_type_id = et.equipment_type_id    
)
SELECT
    equipment_type,
    equipment,
    cost_gp
FROM ranked
WHERE [rank] = 1
ORDER BY equipment_type ASC;
```

Descomponiendo el ejemplo en tres pasos:

1. Devolvemos todos los datos que vamos a utilizar en la consulta:

```sql
SELECT
    et.equipment_type,
    e.equipment,
    e.cost_gp,
FROM equipment e
JOIN equipment_type et ON e.equipment_type_id = et.equipment_type_id
ORDER BY et.equipment_type, e.cost_gp DESC
```

2. Particionamos por tipo (`equipment_type`) y ordenamos por precio (`cost_gp`) en forma descendente:

```sql
SELECT
    et.equipment_type,
    e.equipment,
    e.cost_gp,
    ROW_NUMBER() OVER
    (
        PARTITION BY et.equipment_type
        ORDER BY e.cost_gp DESC
    ) 'rank'
FROM equipment e
JOIN equipment_type et ON e.equipment_type_id = et.equipment_type_id
ORDER BY et.equipment_type, [rank]
```

Como podemos ver, cada grupo tiene las filas rankeadas de acuerdo al ordenamiento de la particion (en este caso, ordenados por costo).

3. Encerramos la consulta en una CTE, y aplicamos un simple `SELECT` para obtener el resultado deseado:

```sql
WITH ranked AS
(
    SELECT
        et.equipment_type,
        e.equipment,
        e.cost_gp,
        ROW_NUMBER() OVER
        (
            PARTITION BY et.equipment_type
            ORDER BY e.cost_gp DESC
        ) 'rank'
    FROM equipment e
    JOIN equipment_type et ON e.equipment_type_id = et.equipment_type_id    
)
SELECT
    equipment_type,
    equipment,
    cost_gp
FROM ranked
WHERE [rank] = 1
ORDER BY equipment_type ASC;
```

# Homework

1. Devolver el personaje mas viejo de cada clase. Consignar nombre de clase, nombre del personaje y edad.
2. Devolver el personaje de cada raza que tenga el arma con mayor daño.