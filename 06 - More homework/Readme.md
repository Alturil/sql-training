# More homework

## `SELECT` exercises

- Seleccionar la cantidad de hinchas que tiene cada equipo (incluyendo los que tengan 0 hinchas).
- Seleccionar la cantidad de equipos sin hinchas que tiene cada conferencia.
- Seleccionar la cantidad de miembros que tiene cada conferencia (considerando el total de miembros que son hinchas de equipos de cada conferencia).
- **Bonus track: The Big JOIN!** Seleccionar nombre, apellido y nacionalidad de cada miembro, mas nombre, pais y conferencia del equipo del que son hinchas.

## `INSERT` exercises

- Insertar un nuevo equipo de Uruguay llamado "Los Guapos de Montevideo", que juegue en la conferencia sudafricana.
- Insertar un miembro nuevo llamado "Mario Baracus", oriundo de Etiopia (Ethiopia), hincha de Sharks, nacido el 6 de Febrero de 1998.
- Insertar todos los miembros de la tabla `new_member` en la tabla `member`.

## `UPDATE` exercises

- Cambiar el apellido de Roberto Carlos a "Goyeneche".
- Cambiar la nacionalidad de todos los miembros nacidos entre 01/Ene/1985 y 10/Feb/1995 para que sean franceses.
- Convertir a todos los hinchas de Crusaders en hinchas de Jaguares.

## `DELETE` exercises

- Borrar a todos los miembros que hayan nacido antes de 01/02/1975.
- Borrar los tres equipos con mas hinchas.
- Borrar a todos los equipos que no tengan hinchas.

## Stored procedure exercises

- Crear un stored procedure que le permita a un usuario obtener el id de un equipo sabiendo su nombre.
- Crear un stored procedure que permita seleccionar nombre y apellido de todos los miembros hinchas de un equipo (el usuario debe proveer el id del equipo en cuestion).
- Crear un stored procedure que le permita a un usuario crear un miembro nuevo, con todas las columnas que existen en la tabla `member`.