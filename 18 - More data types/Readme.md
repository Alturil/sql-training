# More data types

Vamos a complementar el panorama de data types con algunos tipos de datos que no vimos todavia, mas funciones, conversiones y ejemplos de como aplicarlos.

## Numeros enteros

Un principio fundamental a la hora de elegir un tipo de numero entero es balancear las necesidades a mediano y largo plazo en la base de datos con el uso de memoria; los tipos de datos que almacenan mas informacion consumen mas memoria y al tabajar en escala puede ser un problema.

### `BIT`

Puede almacenar `NULL`, `0` o `1` unicamente, por lo cual este tipo de dato se lo utiliza para expresiones booleanas.

La convencio de nombres de variables o columnas booleanas es utilizar un verbo y un atributo, expresado en forma positiva (es decir, asumiendo el valor `1`). Algunos ejemplos:

- `is_enabled` para configuraciones
- `has_discount` en un articulo a la venta

Usualmente las expresiones booleanas en SQL Server son numericas:

```sql
IF (setting.is_enabled = 1)
...
```

Pero en la mayoria de los lenguajes de programacion el uso de la convencios es incluso mas evidente:

```c#
if (Setting.IsEnabled)
{
    ...
}
```

### `TINYINT`

Se utiliza para almacenar numeros entre 0 y 255 (no acepta `NULL` ni numeros negativos). Es util para datos con valores muy predecibles dentro de ese rango. Por ejemplo, el resultado de un partido de futbol, o la edad de un ser humano.

Es importante a la hora de decidir no solamente tener en cuenta los valores que se vayan a almacenar, sino las operaciones que se vayan a hacer con esos valores.

El siguiente ejemplo va a fallar por un error de `arithmetic overflow`:

```sql
DECLARE @valor1 TINYINT = 150;
DECLARE @valor2 TINYINT = 200;

SELECT @valor1 + @valor2;
```

Para solucionar el caso anterior, es posible utilizar la funcion `CAST` en cada uno de los miembros de la operacion, con lo cual la seleccion va a interpretar los valores como `INT`:

```sql
SELECT CAST(@valor1 AS INT) + CAST(@valor2 AS INT);
```

Este tipo de diseño introduce un riesgo, por lo cual no es aconsejable. El mejor lugar para valores de tipo `TINYINT` son representaciones simples sin operaciones ni agregaciones.

### `INT`

A diferencia de los otros tipos, `INT` soporta numeros negativos y tiene un rango de -2,147,483,648 a 2,147,483,647.

Es util para la mayoria de las operaciones por lo cual es practicamente un tipo utilizado por defecto. La unica excepcion es por supuesto valores por encima de 2000 millones, que suele ser un caso muy comun al hacer estadisticas por ejemplo.

## `BIGINT`

Este tipo de datos extiende el rango de `INT`, soportando numeros enteros en un rango de -9,223,372,036,854,775,808 a 9,223,372,036,854,775,807, por lo cual es muy util a la hora de sacar estadisticas, o tabajar con agregaciones en sets muy grandes.

En la mayoria de los lenguajes de programacion este tipo de datos se denomina `long`.

## Numeros decimales

Si bien existen varios tipos, vamos a descartar los tipos "aproximados" (`FLOAT` y `REAL`) porque tienen poca aplicacion practica.

### `DECIMAL`

Es un tipo de datos cuyo uso de memoria varia con la precision asignada. La declaracion toma dos parametros: el primero define la cantidad de digitos totales del valor (_precision_), y el segundo la cantidad de digitos despues de la coma (_scale_).

El siguiente ejemplo almacena la altura de una persona utilizando tres digitos en total, dos de los cuales van despues de la coma:

```sql
DECLARE @altura DECIMAL(3,2) = 1.78
```

El valor maximo de `precision` es 38. El valor de precision por defecto es el maximo (38). Unicamente es posible declarar un valor para `size` si `precision` es declarado primero, y el valor debe ser siempre menor o igual al de `precision`.

Por ejemplo, el siguiente decimal es en terminos practicos el equivalente a un numero entero de hasta 5 digitos, no un decimal con 5 digitos luego de la coma:

```sql
DECLARE @fail DECIMAL(5);
```

A la hora de hacer conversiones automaticas entre numeros decimales, es posible que ocurran errores de overflow, pero tambien truncamientos:

```sql
DECLARE @valor1 DECIMAL(5,2) = 55.55;
DECLARE @valor2 DECIMAL(5,3) = 55.552;

SET @valor1 = @valor2;

SELECT @valor1; -- Devuelve 55.55, no 55.552
```

Otra cosa que sucede por defecto, es que los parametros de `precision` y `size` de los valores de origen en una operacion no determinan los parametros del tipo de datos de resultado:

El resultado del siguiente ejemplo no es `DECIMAL(5,2)`:

```sql
DECLARE @valor1 DECIMAL(5,2) = 10.00;
DECLARE @valor2 DECIMAL(5,2) = 3.00;

SELECT @valor1 / @valor2;
```

Para condicionar el tipo del resultado es posible utilizar `CAST`:

```sql
SELECT CAST((@valor1 / @valor2) AS DECIMAL(5,2));
```

O establecer el tipo de datos para el resultado:

```sql
DECLARE @result DECIMAL(5,2) = (SELECT @valor1 / @valor2);
SELECT @result;
```

## Moneda

Basicamente datos decimales, pero utilizados explicitamente para moneda.

### `SMALLMONEY`

Equivalente a `DECIMAL(10,4)`, tiene un rango de 214,478.3648 a 214,478.3647.

### `MONEY`

[Un excelente tema de Pynk Floyd](https://www.youtube.com/watch?v=cpbbuaIA3Ds), pero ademas tiene un rango de −922,337,203,685,477.5808 a 922,337,203,685,477.5807, para operaciones grandes como agregaciones de capital mundial o caluclo de coimas en Cultura a la Vuelta.

```sql
DECLARE @billetera MONEY = 53.50;
DECLARE @ladri DECIMAL (5,3) = 13.5;

SELECT @billetera - @ladri;
```

## Texto

Los dos tipos de datos de texto que vamos a utilizar soportan caracteres unicode (por eso la `N` antes del nombre del tipo de dato).

### `NCHAR`

Una secuencia de caracteres unicode de longitud fija. Es necesario consignar la cantidad de caracteres que va a tener el texto en la declaracion.

A diferencia de `NVARCHAR`, en caso de proveer una cantidad de caracteres menor a la longitud establecida, los caracteres restantes se completan automaticamente con espacios.

Para demostrar esto vamos a utilizar la funcion `CONCAT` que nos permite concatenar texto. Recibe como parametros una lista con todas las porciones de texto que se deseen concatenar. El resultado del siguiente ejemplo llena con espacios la variable `@nombre` hasta completar los 50 caracteres establecidos en la declaracion:

```sql
DECLARE @nombre NCHAR(50) = 'Alturil';
SELECT CONCAT('|',@nombre,'|');
```

Hay pocos casos en los que este tipo de datos es util, y en general tiene que ver con procesos internos dentro de SQL 

### `NVARCHAR`

La parte de `VAR` en el nombre del tipo de datos indica que la cantidad de caracteres es variable, es decir que el valor de esta variable puede tener como maximo la cantidad de caracteres declarada.

Si repetimos el ejemplo anterior, por ejemplo, vamos a ver que los dos `|` aparecen inmediatemente antes y despues del valor:

```sql
DECLARE @nombre NVARCHAR(50) = 'Alturil';
SELECT CONCAT('|',@nombre,'|');
```

Existen muchas funciones para manipular texto, pero las mas utiles a la hora de manipular datos son `CONCAT` y `REPLACE`. Esta ultima nos permite reemplazar todas la ocurrencias de una porcion de texto con otro valor:

```sql
SELECT REPLACE('Frodo es el mejor hobbit, viva Frodo!', 'Frodo', 'Bilbo');
```

Adicionalmente podemos utilizar el operador `LIKE` en conjunto con el wildcard `%` para seleccionar filas que contengan una porcion de texto. Por ejemplo:

```sql
-- Seleccionar todos los personajes que comiencen con la letra A
SELECT * FROM [character] WHERE [name] LIKE 'A%';

-- Seleccionar los paises que terminene con 'Korea'
SELECT * FROM country WHERE country LIKE '%Korea';

-- Seleccionar los paises que tengan 'New' en alguna parte del nombre
SELECT * FROM country WHERE country LIKE '%New%';
```

## Fechas

### `DATE`

Representa una fecha (año, mes, dia), sin la porcion que indica el tiempo (horas, minutos, segundos, milisegundos). Para declarar un valor explicitamente se utiliza texto en un formato especifico: `YYYY-MM-DD`:

```sql
DECLARE @fecha_de_nacimiento DATE = '1987-09-29';
SELECT @fecha_de_nacimiento;
```

Es importante no olvidarse de que las fechas se almacenan como un objeto independientemente del formato. Por lo general el formateo sucede en la aplicacion que utiliza la base de datos, pero en caso de ser necesario puede hacerse directamente con SQL utilizando la funcion `FORMAT`:

```sql
DECLARE @fecha_de_nacimiento DATE = '1987-09-29';
-- En criollo:
SELECT FORMAT(@fecha_de_nacimiento, 'dd/MMM/yyyy') AS DATE;
-- Mes como texto:
SELECT FORMAT(@fecha_de_nacimiento, 'MMMM dd, yyyy') AS DATE;

```

### `DATETIME`

Es una representacion de tiempo mas granular que `DATE` ya que contiene informacion sobre el tiempo (hora, minuto, segundos, milisegundos).

Existen funciones de fecha que resultan muy utiles a la hora de trabajar con bases de datos. Una de ellas es `GETDATE`, que nos devuelve la fecha actual, utilizando la fecha del servidor en el que corre la base de datos (en nuestro caso, la zona horaria es UTC 0). Al menos que se indique lo contrario, la funcion devuelve un timestamp, es decir, `DATETIME`.

```sql
SELECT GETDATE();
```

Otra funcion util es `DATEADD`, que permite agregar o sustraer tiempo a una fecha determinada. Se utiliza mucho para rangos relativos:

```sql
-- La hora hace exactamente dos horas
SELECT DATEADD(HOUR, -2, GETDATE());

-- La fecha una semana despues de mi cumpleaños
DECLARE @my_birthday DATE = '2020-09-29';
SELECT DATEADD(DAY, 7, @my_birthday);
```

Por ultimo, `DATEDIFF` calcula la diferencia entre dos fechas. Al igual que el resto de las funcions de fecha, SQL tiene en cuenta el calendario, lo cual resulta sumamente util para no tener que hacer excepciones si quisieramos calucular matematicamente sin este contexto:

```sql
-- Cuanto falta para mi cumpleaños?
DECLARE @my_birthday DATE = '2020-09-29';
SELECT DATEDIFF(DAY, GETDATE(), @my_birthday);
```