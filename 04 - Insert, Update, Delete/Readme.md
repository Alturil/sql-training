# Insert, Update, Delete

Tres operaciones que nos permiten manipular datos en una base de datos sql:

- `INSERT` para agregar nuevos registros.
- `UPDATE` para actualizar registros.
- `DELETE` para borrar registros.

## Insert

Permite agregar registros en la tabla especificada. Las dos formas mas comunes son:

### Consignando valores literales:

Especificando el nombre de la tabla, las columnas a insertar, y cada fila nueva con los valores que le correspondan a cada columna, respetando el orden consignado en el `INSERT`:

```sql
INSERT INTO
    <tabla> (<columna_1>, ... <columna_n>)
VALUES
    (<valor_columna1>, ... <valor_columna_n>),
    (<valor_columna1>, ... <valor_columna_n>);
```

Es importante conocer la estructura de la tabla a donde estamos insertando, especialmente lo siguiente:

- Tipo de datos que acepta cada columna
- Columnas cuyo valor se asigna automaticamente (ej: `IDENTITY`)
- Columnas que aceptan `NULL`

**Ej:** Agregar un nuevo miembro:

```sql
INSERT INTO [member] (first_name, last_name)
VALUES ('Anabella', 'Ascar');
```

### Utilizando el resultado de otra query

**Ej:** Agregar los tres primeros miembros de `new_member` a `member`:

```sql
INSERT INTO member (
    first_name,
    last_name,
    date_of_birth,
    nationality_id,
    supported_team_id
    )
SELECT TOP 3
    first_name,
    last_name,
    date_of_birth,
    nationality_id,
    supported_team_id
FROM new_member;
```

## Update

Permite modificar registros en la tabla especificada.

**Ej:** Convertir a todos los afganos en argentinos:

```sql
UPDATE member
SET nationality_id = 7 -- Argentina
WHERE nationality_id = 1; -- Afghanistan
```

O usando sub-queries:

```sql
UPDATE member
SET nationality_id = (  SELECT country_id FROM country
                        WHERE country = 'Argentina')
WHERE nationality_id = (SELECT country_id FROM country
                        WHERE country = 'Afghanistan');
```

**Pro-tip:** Escribir el `WHERE` antes que el resto de la query, para evitar actualizar TODOS los registros de la tabla accidentalmente!

## Delete

Permite borrar registros en la tabla especificada:

### Usando una query

**Ej:** Borrar a todos los hinchas de Crusaders:

```sql
DELETE FROM member
WHERE supported_team_id = 9 -- Crusaders
```

Tambien se pueden usar subqueries aca.

### Usando un join

**Ej:** Borrar a todos los hinchas de Crusaders:

```sql
DELETE m
FROM member m
JOIN team t ON m.supported_team_id = t.team_id
WHERE t.name = 'Crusaders';
```

**Pro-tip:** Escribir el `WHERE` antes que el resto de la query, para evitar borrar TODOS los registros de la tabla accidentalmente!

## Bonus track: variables

Se pueden utilizar variables que ayudan a encadenar queries:

**Ej:** Convertir un miembro en hincha de Crusaders, seleccionar el registro actualizado y volverlo a su equipo original:

```sql
-- Obtener team_id de Crusaders
DECLARE @new_supported_team_id INT = 
    (SELECT team_id FROM team
     WHERE name = 'Crusaders');

-- Obtener un miembro que NO sea hincha de Crusaders
DECLARE @member_id INT = 
    (SELECT TOP 1 member_id FROM member
     WHERE supported_team_id <> @new_supported_team_id);

-- Obtener team_id del equipo del cual el miembro es hincha
DECLARE @old_supported_team_id INT = 
    (SELECT supported_team_id FROM member
     WHERE member_id = @member_id);

-- Hacer que el miembro sea hincha de Crusaders
UPDATE member
SET supported_team_id = @new_supported_team_id
WHERE member_id = @member_id;

    -- Controlo con un Select
    SELECT m.first_name, m.last_name, t.name
    FROM member m
    JOIN team t ON m.supported_team_id = t.team_id
    WHERE member_id = @member_id;

-- Revertir el hincha a su equipo anterior
UPDATE member
SET supported_team_id = @old_supported_team_id
WHERE member_id = @member_id;

    -- Controlo con un select
    SELECT m.first_name, m.last_name, t.name
    FROM member m
    JOIN team t ON m.supported_team_id = t.team_id
    WHERE member_id = @member_id;
```

# Homework

- No mentir en los comentarios de Git