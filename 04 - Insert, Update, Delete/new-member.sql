-- Re-create table [new_member]

DROP TABLE new_member;

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[new_member](
	[new_member_id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](1000) NULL,
	[surname] [nvarchar](1000) NULL,	
	[country_id] [int] NULL,
	[favourite_team_id] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[new_member] ADD  CONSTRAINT [PK_new_member] PRIMARY KEY CLUSTERED 
(
	[new_member_id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY]
GO

-- Populate [new_member] with data

INSERT INTO [dbo].[new_member](
	[name],
	[surname],	
	[country_id],
	[favourite_team_id])
VALUES
('Carlos', 'Bala', NULL, NULL),
('Roberto', 'Galan', 2, NULL),
('Cristian', 'Castro', 1, 7),
('Baila Tu Cuerpo', 'Alegria Macarena', NULL, 4);