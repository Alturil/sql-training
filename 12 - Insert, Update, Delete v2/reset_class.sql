SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE OR ALTER PROCEDURE [dbo].[reset_class]
AS
    DROP TABLE IF EXISTS [class];

    CREATE TABLE class(
        [class_id] [int] IDENTITY(1,1) NOT NULL,
        [class] [nvarchar](1000) NOT NULL
    ) ON [PRIMARY];
    
    ALTER TABLE [dbo].[class] ADD  CONSTRAINT [PK_class] PRIMARY KEY CLUSTERED 
    ([class_id] ASC) WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY];

    INSERT INTO class (class)
    VALUES
    ('Fighter'),
    ('Paladin'),
    ('Barbarian'),
    ('Mage'),
    ('Bard'),
    ('Cleric'),
    ('Druid'),
    ('Ranger'),
    ('Monk'),
    ('Rogue');    
GO
