SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE OR ALTER PROCEDURE [dbo].[reset_weapon]
AS
    DROP TABLE IF EXISTS [weapon];

    CREATE TABLE weapon(
        [weapon_id] [int] IDENTITY(1,1) NOT NULL,
        [weapon] [nvarchar](1000) NOT NULL,
        [weapon_type_id] INT NOT NULL,
        [damage] INT NOT NULL,
        [critical_multiplier] INT NOT NULL
    ) ON [PRIMARY];
    
    ALTER TABLE [dbo].[weapon] ADD  CONSTRAINT [PK_weapon] PRIMARY KEY CLUSTERED 
    ([weapon_id] ASC) WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY];

    INSERT INTO weapon (weapon, weapon_type_id, damage, critical_multiplier)
    VALUES
    ('Unarmed',	1, 1, 2),
    ('Gauntlet', 1, 2, 2),
    ('Dagger', 2, 3, 2),
    ('Sword', 3, 5, 2),
    ('Morningstar', 6, 5, 2),
    ('Mace', 3, 5, 2),
    ('Pike', 3, 4, 2),
    ('Axe', 3, 5, 2),
    ('Spear', 4, 7, 2),
    ('Double axe', 4, 8, 3),
    ('Warhammer', 4, 9, 3),
    ('Greatsword', 7, 5, 3),
    ('Bow', 5, 5, 3),
    ('Crossbow', 5, 6, 2);
GO