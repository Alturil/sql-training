SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE OR ALTER PROCEDURE [dbo].[reset_character_weapon]
AS
    DROP TABLE IF EXISTS [character_weapon];

    CREATE TABLE character_weapon(
        [character_weapon_id] [int] IDENTITY(1,1) NOT NULL,
        [character_id] [int] NOT NULL,
        [weapon_id] [int] NOT NULL
    ) ON [PRIMARY];
    
    ALTER TABLE [dbo].[character_weapon] ADD  CONSTRAINT [PK_character_weapon] PRIMARY KEY CLUSTERED 
    ([character_weapon_id] ASC) WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY];

    INSERT INTO character_weapon (character_id, weapon_id)
    VALUES
    (1, 4),
    (1, 3),
    (1, 14),
    (2, 13),
    (3, 8),
    (4, 13),
    (5, 4),
    (6, 3),
    (7, 1),
    (8, 2),
    (9, 3),
    (10, 6),
    (11, 9),
    (12, 14),
    (13, 10),
    (14, 7),
    (15, 3),
    (16, 11),
    (18, 11),
    (19, 2),
    (20, 14),
    (11, 4),
    (23, 1),
    (23, 3),
    (21, 6),
    (13, 4),
    (22, 1),
    (6, 13),
    (2, 3),
    (12, 10),
    (3, 11);

GO