SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE OR ALTER PROCEDURE [dbo].[reset_weapon_type]
AS
    DROP TABLE IF EXISTS [weapon_type];

    CREATE TABLE weapon_type(
        [weapon_type_id] [int] IDENTITY(1,1) NOT NULL,
        [weapon_type] nvarchar(1000) NOT NULL
    ) ON [PRIMARY];
    
    ALTER TABLE [dbo].[weapon_type] ADD  CONSTRAINT [PK_weapon_type] PRIMARY KEY CLUSTERED 
    ([weapon_type_id] ASC) WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY];

    INSERT INTO weapon_type (weapon_type)
    VALUES
    ('Unarmed'),
    ('Light melee'),
    ('One-handed'),
    ('Two-handed'),
    ('Ranged');
GO