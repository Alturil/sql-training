SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE OR ALTER PROCEDURE [dbo].[reset_character]
AS
    DROP TABLE IF EXISTS [character];

    CREATE TABLE character(
        [character_id] [int] IDENTITY(1,1) NOT NULL,
        [name] NVARCHAR(1000) NOT NULL,
        [class_id] INT NOT NULL,
        [race_id] INT NOT NULL,
        [age] INT NULL,
        [experience] INT NULL
    ) ON [PRIMARY];
    
    ALTER TABLE [dbo].[character] ADD  CONSTRAINT [PK_character] PRIMARY KEY CLUSTERED 
    ([character_id] ASC) WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY];

    INSERT INTO [character] ([name], class_id, race_id, age, experience)
    VALUES
    ('Alturil', 1, 1, 31, 64000),
    ('Luthychan', 8, 2, 132, 72320),
    ('Bardin', 1, 3, 38, 2699),
    ('Kerillian', 8, 2, 512, 162020),
    ('Boromir', 2, 1, 35, 79014),
    ('Tauriel', 8, 2, 432, 5555),
    ('Gandalf', 4, 1, 762, 355000),
    ('Radagast', 7, 1, 813, 306578),
    ('Bilbo', 10, 6, 55, 36087),
    ('Azog', 3, 5, 42, 250400),
    ('Don Segundo', 2, 1, 35, 123700),
    ('Ardie Savea', 3, 5, 27, 357),
    ('Conan', 3, 1, 22, 122),
    ('Rumpelstinski', 4, 7, 67, 49154),
    ('Marcelo', 4, 1, 31, 197750),
    ('Brian May', 5, 7, 75, 100050),
    ('Horacio Guarani', 5, 3, 84, 7000),
    ('Durin', 6, 3, 483, 9600),
    ('Bergoglio', 6, 5, 72, 20),
    ('Rumzeis', 7, 4, 26, 280000),
    ('Krillin', 9, 6, 37, 0),
    ('Dalai Lama', 9, 7, 92, 49050),
    ('Kamkuru', 10, 4, 26, 302050);

GO