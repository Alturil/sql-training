SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE OR ALTER PROCEDURE [dbo].[reset_race]
AS
    DROP TABLE IF EXISTS [race];

    CREATE TABLE race(
        [race_id] [int] IDENTITY(1,1) NOT NULL,
        [race] [nvarchar](1000) NOT NULL,
        [carrying_capacity_lb] INT NOT NULL
    ) ON [PRIMARY];
    
    ALTER TABLE [dbo].[race] ADD  CONSTRAINT [PK_race] PRIMARY KEY CLUSTERED 
    ([race_id] ASC) WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY];

    INSERT INTO race (race, carrying_capacity)
    VALUES
    ('Human', 100),
    ('Elf', 80),
    ('Dwarf', 170),
    ('Gnome', 60),
    ('Orc', 150),    
    ('Halfling', 70),
    ('Half-elf', 90);
GO
