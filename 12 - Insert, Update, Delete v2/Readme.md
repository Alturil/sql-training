# Insert, Update, Delete v2

Los siguientes comandos nos sirven para agregar, modificar o borrar filas:

- `INSERT` para agregar nuevas filas
- `UPDATE` para modificar los valores de una o mas filas
- `DELETE` para borrar filas

# `INSERT`

Se usa para insertar una o multiple filas dentro de una tabla. Las dos formas mas comunes de realizar esta operacion son:

## Utilizando valores literales

Se utiliza cuando los datos a insertar no tienen relacion con el resultado de una consulta.

```sql
INSERT INTO
    [tabla] ([columna_1], ... [columna_n])
VALUES
    ([valor_columna]>, ... [valor_columna_n]),
    ([valor_columna]>, ... [valor_columna_n]);
```

**Ej:** Agregar `Halberd` y `Whip` a la lista de armas:

```sql
INSERT INTO
    [weapon] (weapon, weapon_type_id, damage, critical_multiplier)
VALUES
    ('Halberd', 4, 6, 3),
    ('Whip', 3, 3, 2);
```

## Utilizando el resultado de otra consulta

Para el siguiente ejemplo vamos a insertar esas dos mismas armas en una variable de tipo tabla, y luego vamos a insertar en la tabla `weapon` las dos armas utilizando un select a la variable de tipo tabla:

```sql
-- Declaramos la variable de tipo tabla
DECLARE @new_weapons TABLE
(
    weapon NVARCHAR(1000),
    weapon_type_id INT,
    damage INT,
    critical_multiplier INT
);

-- Insertamos las dos armas nuevas en @new_weapons
INSERT INTO
    @new_weapons (weapon, weapon_type_id, damage, critical_multiplier)
VALUES
    ('Halberd', 4, 6, 3),
    ('Whip', 3, 3, 2);

-- Insertamos las armas de @new_weapons en [weapon]
INSERT INTO
    weapon (weapon, weapon_type_id, damage, critical_multiplier)
SELECT weapon, weapon_type_id, damage, critical_multiplier
FROM @new_weapons;
```

Por supuesto que no es la solucion mas practica para este caso, pero lo es en caso de que la informacion se encuentre en otras tablas inicialmente.

# `UPDATE`

Se utiliza para modificar los valores de una o mas columnas en las filas especificadas. La sintaxis es bastante sencilla:

```sql
UPDATE [tabla]
SET [columna_1] = [valor_nuevo], ..., [columna_n] = [valor_n]
WHERE [condiciones];
```

**Ej:** Cambiar los valores de `damage` y `critical_multiplier` del arma `Halberd`:

```sql
UPDATE weapon
SET damage = 5, critical_multiplier = 2
WHERE weapon = 'Halberd';
```

Es **FUNDAMENTAL** escribir la clausula `WHERE` desde un principio para evitar actualizar todas las filas de la tabla accidentalmente.

# `DELETE`

Se utiliza pra borrar filas en una tabla. La sintaxis es muy parecida a un `SELECT`, por lo cual se recomienda escribir la consulta como un `SELECT` al principio, y luego transformarla en un `DELETE`.

```sql
DELETE FROM [tabla]
WHERE [condicion]
```

**Ej:** Borrar las dos armas que agregamos anteriormente (`Halberd` y `Whip`):

```sql
DELETE FROM weapon
WHERE weapon IN ('Halberd', 'Whip');
```

Al igual que con `UPDATE`, es **FUNDAMENTAL** escribir la clausula `WHERE` desde un principio para evitar actualizar todas las filas de la tabla accidentalmente.

# Homework

Para cada una de las consignas a continuacion, realizar los siguientes pasos para que el script provea verificacion paso por paso y restauracion del estado original de los datos al final.

1. Construir los `SELECT` para verificar el _before_ y _after_
2. Agregar la ejecucion del stored procedure para resetear la tabla al final
3. Escribir el comando para resolver el ejercicio
4. Extraer el input en variables donde sea posible

**Ej:** Borrar el arma `Axe` de la tabla `weapon`:

```sql
DECLARE @weapon NVARCHAR(1000) = 'Axe';

-- Before
SELECT * FROM weapon
WHERE weapon = @weapon;

-- Delete
DELETE FROM weapon
WHERE weapon = @weapon;

-- After
SELECT * FROM weapon
WHERE weapon = @weapon;

-- Reset table
EXECUTE reset_weapon;
```

Existe un stored procedure para resetear cada tabla:

| Tabla | Stored procedure |
|-|-|
|character|reset_character|
|race|reset_race|
|class|reset_class|
|weapon|reset_weapon|
|weapon_type|reset_weapon_type|
|character_weapon|reset_character_weapon|

## Consignas

- Crear un personaje, asignarle raza, clase, y tres armas distintas.
- Realizar una consulta que traiga todos los datos del personaje creado, incluyendo el nombre de la raza y el nombre de la clase (no incluir armas).
- Crear una clase nueva, y cambiar la clase de `Gandalf` a esta clase.
- Borrar todos los peronajes que tengan menos de 100 años.
- Insertar en una temprary table las tres profesiones mas populares del juego.