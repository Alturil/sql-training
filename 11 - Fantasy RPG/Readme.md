# Fantasy RPG

Vamos a empezar a construir una base de datos para un juego de rol de fantasia. Hay algunas tablas ya creadas en la base de datos que vamos a ir ampliando en las semanas que vienen. Los nombres de las tablas son:

- `character`: Contiene los personajes del juego
- `race`: Razas del juego
- `class`: Clases o profesiones que puede tener un personaje
- `weapon`: Armas disponibles
- `weapon_type`: Tipos de armas
- `character_weapon`: Personajes y que arma/s usa cada uno

# Homework

Crear un diagrama de esa base de datos usando https://dbdiagram.io/, que contenga:

- Todas las tablas
- Sus respectivas columnas y tipos de datos
- La clave primaria de cada una
- Las relaciones entre las tablas

Pueden usar como ejemplo lo que ya tenemos hecho para members: https://dbdiagram.io/d/5e01598cedf08a25543f5e49