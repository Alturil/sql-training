# 09 - CASE y CTE

## CASE

Esta expresion permite devolver resultados distintos en base a la evaluacion de una o mas condiciones.

```sql
CASE [ dato_de_entrada ]
    WHEN condicion1 THEN resultado1
    WHEN condicion2 THEN resultado2
    ...    
    [ ELSE resultado por defecto ]
END  
```

Donde:
- `CASE` y `END` delimitan la expresion de tipo `CASE`
- `[dato_de_entrada]` es opcional y aplica en caso de que todas las condiciones evaluen el mismo dato de entrada
- `WHEN` precede a la condicion a evaluar, y `THEN` especifica el resultado a devolver en caso de que la condicion se cumpla
- `[ELSE resultado por defecto]` denota el resultado que se devuelve en caso de no cumplirse ninguna condicion anterior

**Ej:** Devolver todos los partidos de la primera ronda de Super Rugby consignando el nombre de los equipos, el puntaje de cada uno, y el nombre del equipo ganador en cada partido.

```sql
SELECT
    th.name 'Home team',
    m.home_team_score 'Home team score',
    tv.name 'Visiting team',
    m.visiting_team_score 'Visiting team score',
    CASE
        WHEN m.home_team_score > m.visiting_team_score THEN th.name
        ELSE tv.name
    END 'Winning team'
FROM [match] m
JOIN team th ON m.home_team_id = th.team_id
JOIN team tv ON m.visiting_team_id = tv.team_id
WHERE m.[round] = 1;
```

## Common Table Expressions (CTE)

Este tipo de expresiones nos permiten transformar tempralmente un resultado en una tabla para ser utilizado en la operacion subsiguiente. El uso mas comun es disponer los datos de modo tal que luego puedan ser agregados. La sintaxis basica se define:

```sql
WITH nombre_de_expresion [(columna1 [,...])]
AS
    (definicion_de_expresion)
SQL_statement;
```

Donde:
- `WITH` define la utilizacion de una CTE
- `nombre_de_expresion` es el nombre que se le asigna, opcionalmente definiendo columnas entre parentesis
- `AS` antecede a la definicion de la expresion, que se expresa entre parentesis.
- `SQL_statement` es simplemente la operacion posterior que utiliza la CTE como base.

El ejemplo final que vimos prepara los datos de los partidos que jugo Jaguares a lo largo del torneo para luego permitirnos obtener informacion agregada, puntualmente:

- Cual es el promedio de diferencia de puntos por partido?
- Cual es el maximo puntaje anotado en un partido?
- A cuantos equipos distintos enfrento en el torneo?

```sql
DECLARE @team NVARCHAR(1000) = 'Jaguares';

WITH cte AS
--DECLARE @team NVARCHAR(1000) = 'Jaguares';
(SELECT
    m.[round] 'round',
    @team 'our_team',
    CASE
        WHEN th.name = @team THEN m.home_team_score
        ELSE m.visiting_team_score
    END 'our_score',    
    CASE
        WHEN th.name = @team THEN tv.name
        ELSE th.name
    END 'rival_team',    
    CASE
        WHEN tv.name = @team THEN m.home_team_score
        ELSE m.visiting_team_score
    END 'rival_score',    
    CASE
        WHEN
            (th.name = @team AND m.home_team_score > m.visiting_team_score)
         OR (tv.name = @team AND m.visiting_team_score > m.home_team_score)
        THEN 'Win' ELSE 'Loss'
    END 'result',
    CASE
        WHEN th.name = @team THEN 'Home' ELSE 'Visitor'
    END 'condition',
    ABS(m.home_team_score - m.visiting_team_score) 'difference'
FROM [match] m
JOIN team th ON m.home_team_id = th.team_id
JOIN team tv ON m.visiting_team_id = tv.team_id
WHERE th.name = @team OR tv.name = @team)

SELECT
    AVG([difference]) 'Avg points difference',
    MAX(our_score) 'Max points scored',
    COUNT(DISTINCT rival_team) 'Amount of rival teams'
FROM cte
```

# Homework

- Contar cuantos paises en cada una de estas categorias hay en el mundo:
    - Chicos: poblacion menor a 10 millones de personas
    - Medianos: poblacion entre 10 y 80 millones
    - Grandes: poblacion mayor a 80 millones
- Ordenar a los equipos de Super Rugby de mas ganador a mas perdedor, visualizando unicamente el nombre de los equipos y la cantidad total de victorias en todo el torneo
- Teniendo en cuenta los paises representados en el Super Rugby, calcular el area total de esos paises combinados, pero agrupados por continente (por ejemplo, Nueva Zelanda y Australia estan en el mismo continente, por lo cual sus areas se sumarian, no asi la de Sudafrica que esta en otro continente).